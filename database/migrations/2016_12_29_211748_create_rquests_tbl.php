<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRquestsTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sign_in_id')->nullable();
            $table->integer('emp_id')->nullable();
            $table->string('type')->nullable(); // Normal Leaves
            $table->integer('status')->default(2);
            $table->date('req_day')->nullable();
            $table->datetime('time_from')->nullable();
            $table->datetime('time_to')->nullable();
            $table->datetime('actual_time_from')->nullable();
            $table->datetime('actual_time_to')->nullable();
            $table->time('interval_in_time')->nullable();
            $table->float('interval_in_days')->nullable();
            $table->string('add_info')->nullable();
            $table->integer('taken')->default(0);
            $table->timestamps();
        });  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('requests');
    }
}
