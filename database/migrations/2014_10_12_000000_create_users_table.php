<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->string('password');
            $table->string('dec_password');
            $table->string('user_type')->nullable();
            $table->integer('admin_show')->default(0);
            $table->string('phone');
            $table->integer('office_id');
            $table->integer('company_id');
            $table->integer('department_id');
            $table->integer('manager_id');
            $table->integer('att_profile_id');
            $table->integer('role_id');
            $table->integer('position_id');
            $table->integer('can_work_home');
            $table->integer('max_homeDays_per_week');
            $table->integer('flexible_home');
            $table->integer('can_ex_days');
            $table->string('home_days');
            
            $table->rememberToken();
            $table->timestamps();
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
        
    }
}
