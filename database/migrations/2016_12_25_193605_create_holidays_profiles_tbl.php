<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHolidaysProfilesTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('holidays_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('holiday_id');
            $table->integer('holiday_profile_id');
            $table->integer('admin_show');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('holidays_profiles');    
    }
}
