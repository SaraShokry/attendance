<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttProfileTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('att_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('profile_type')->default('Normal'); // Normal, Holiday, Half day
            $table->integer('work_on_holiday_profile_id')->nullable();
            $table->integer('first_half_day_profile_id')->nullable();
            $table->integer('second_half_day_profile_id')->nullable();
            $table->integer('company_id')->nullable();
            $table->integer('office_id')->nullable();
            $table->string('name')->nullable();
            $table->integer('work_status_id')->nullable();
            $table->integer('sign_in_req')->nullable();
            $table->time('time_allowed_before_sign_in')->nullable();
            $table->time('sign_in_start_time')->nullable();
            $table->time('sign_in_end_time')->nullable();
            $table->time('sign_out_cuttoff_time')->nullable();
            $table->time('end_of_day')->nullable();
            $table->time('work_hours')->nullable();
            $table->string('early_signout_action')->nullable();
            $table->float('early_signout_deduction_days')->nullable();
            $table->string('early_signout_deduction_type')->nullable();
            $table->string('weekends')->nullable();
            $table->integer('holiday_profile_id')->nullable();
            $table->float('allowed_normal_leaves')->nullable();
            $table->time('normal_leave_cuttoff')->nullable();
            $table->integer('allow_half_day')->nullable();
            $table->integer('allowed_emergency_leaves')->nullable();
            $table->integer('emergency_leave_without_permission')->nullable();
            $table->float('emergency_penalty')->nullable();
            $table->string('emergency_penalty_type')->nullable();
            $table->integer('overtime_permissible')->nullable();
            $table->time('min_overtime_cuttoff')->nullable();
            $table->integer('overtime_limit')->nullable();
            $table->time('max_overtime_per_day')->nullable(); // hours
            $table->string('max_overtime_per_month')->nullable(); // hours
            $table->float('overtime_payment_factor')->nullable(); 
            $table->time('max_lateness_permissibility')->nullable(); //minutes
            $table->time('minor_tardiness_range')->nullable(); // minutes
            $table->time('major_tardiness_range')->nullable(); // minutes
            $table->float('minor_tardiness_penalty')->nullable();
            $table->string('minor_tardiness_penalty_type')->nullable();
            $table->float('major_tardiness_penalty')->nullable();
            $table->string('major_tardiness_penalty_type')->nullable();
            $table->time('min_permission_unit')->nullable(); // mintues
            $table->integer('max_units_per_day')->nullable();
            $table->integer('max_units_per_month')->nullable();
            $table->integer('can_work_holiday')->nullable();
            $table->float('holiday_factor')->nullable();
            $table->float('extra_holiday_hour_factor')->nullable();
            $table->integer('week_start_day')->nullable();
            $table->integer('admin_show')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('att_profiles');
    }
}
