<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSigninTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sign_in', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('emp_id')->nullable();
            $table->date('date')->nullable();
            $table->datetime('sign_in_time')->nullable();
            $table->datetime('sign_out_time')->nullable();
            $table->string('status')->nullable();//on time , minor tardiness, major tardiness  , Weekend, Leave, Holiday
            $table->string('early_late')->nullable();// early, late
            $table->time('early_late_time')->nullable();
            // Sign out: on time, early -> Leeha 7war , late->with overtime (within overtime permissibile limit(daily, monthly)) , late->without overtome = ontime , 
            $table->string('day_type')->nullable();
            $table->integer('from_home')->nullable();
            $table->integer('attended')->nullable();
            $table->datetime('calculated_min_sign_out_time')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sign_in');
    }
}
