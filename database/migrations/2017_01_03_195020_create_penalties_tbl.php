<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenaltiesTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penalties', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sign_in_id')->nullable();
            $table->datetime('date')->nullable();
            $table->integer('company_id')->nullable();
            $table->integer('office_id')->nullable();
            $table->integer('emp_id')->nullable();
            $table->float('quantity')->nullable();//days
            $table->string('deducted_from')->nullable();//Leaves,Salary
            $table->text('penalty_reason')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('penalties');
    }
}
