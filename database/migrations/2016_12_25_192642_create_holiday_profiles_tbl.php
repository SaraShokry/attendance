<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHolidayProfilesTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('holiday_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id');
            $table->integer('office_id');
            $table->string('name');
            $table->integer('admin_show');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('holiday_profiles');
    }
}
