<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Company;
use App\Employee_role;
use App\Department;
use App\Position;
use App\Att_profile;
use App\Office;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        // Admin
        $user = new User();
        $user->name = 'Admin';
        $user->email = 'mtarek@arabiclocalizer.com';
        $user->password = bcrypt('123456');
        $user->user_type = 'Admin';
        $user->save();
        // Create Roles
        $role = new Employee_role();
        $role->name = "Owner";
        $role->admin_show = 1;
        $role->save();
        $role = new Employee_role();
        $role->name = "HR";
        $role->admin_show = 1;
        $role->save();
        $role = new Employee_role();
        $role->name = "Manager";
        $role->admin_show = 1;
        $role->save();
        $role = new Employee_role();
        $role->name = "Employee";
        $role->admin_show = 1;
        $role->save();
        // create Office
        $office = new Office();
        $office->company_id = 1;
        $office->name = "Alex Branch";
        $office->save();
        // Create departments
        $department = new Department();
        $department->company_id = 1;
        $department->office_id = 1;
        $department->name = "HR";
        $department->admin_show = 1;
        $department->save();

        $department = new Department();
        $department->company_id = 1;
        $department->office_id = 1;
        $department->name = "Translation";
        $department->admin_show = 1;
        $department->save();
        // Create positions
        $position = new Position();
        $position->company_id = 1;
        $position->office_id = 1;
        $position->name = "HR Head";
        $position->admin_show = 1;
        $position->save();

        $position = new Position();
        $position->company_id = 1;
        $position->office_id = 1;
        $position->name = "Translator";
        $position->admin_show = 1;
        $position->save();
        // Create Profiles

        $att_profile = new Att_profile();
        $att_profile->company_id = 1;
        $att_profile->office_id = 1;
        $att_profile->name = "Default";
        $att_profile->admin_show = 1;
        $att_profile->save();
        // Company
        $company = new Company();
        $company->name = 'Arabic Localizer';
        $company->admin_show = 1;
        $company->save();

        // Partner
        $user = new User();
        $user->name = 'Mohamed Tarek';
        $user->email = 'mtarek1@arabiclocalizer.com';
        $user->password = bcrypt('123456');
        $user->user_type = 'Owner';
        $user->admin_show = 1;
        $user->role_id = 1;
        $user->company_id = 1;
        $user->save();
        
    }
}
