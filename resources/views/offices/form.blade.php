<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i> New  </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form action="{{route('admin.offices.save')}}" method="post" class="ajax_form">
        <div  id="main-form" novalidate method="POST" class="larajsval form"> 
            <div class="form-body">
            <div class="mt-element-ribbon bg-grey-steel">
                                                <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                                                    <div class="ribbon-sub ribbon-clip"></div> Main Info </div>
                                                <p class="ribbon-content">
                                                <div class="form-group">
                                                  <div class="form-group">
                    <label>Name</label>
                    <input name="name" class="form-control" data-validation="required" data-name="Name" value="{{$office->name}}" >
                    <span class="error_message"></span>
                    <input type="hidden" name="id" id="hiddenField" value="{{$office->id}}" data-validation="" data-name="id" />
                    <input type="hidden" name="company_id" id="company_id" value="{{Auth::user()->getCompany->id}}" data-validation="" data-name="company_id" />
                    <input type="hidden" name="time_zone1" id="time_zone1" value="{{$office->time_zone}}" data-validation="" data-name="time_zone1" />
                                                  </div>
                                                  <div class="form-group">
                    <label>Time Zone</label>
                    <select name="time_zone" id="time_zone" data-validation="required" data-name="Time zone" class="form-control">
                    	<option value="">Select Time Zone</option>
                        @foreach($list_items as $item)
                        	<option value="{{$item['name']}}">{{$item['name']}} ({{$item['offset']}})</option>
                        @endforeach
                    </select>
                    <span class="error_message"></span>
                </div>
                <div class="form-group">
                    <label>Head Quarter</label>
                    <select name="head_quarter" data-validation="required" data-name="Head Quarter" class="form-control">
                    	<option value="">Select Option</option>
                    	<option value="0" <?php if($office->head_quarter == 0){?>selected<?php ;}?>>No</option>
                    	<option value="1" <?php if($office->head_quarter == 1){?>selected<?php ;}?>>Yes</option>
                    </select>
                    <span class="error_message"></span>
                </div>
                <div class="form-group">
                    <label>Address</label>
                    <input name="address" class="form-control" data-validation="required" data-name="Address" value="{{$office->address}}" >
                    <span class="error_message"></span>
                </div>
                
                
                
                
                </p>
                
                                            </div>
                                            
                                            </div>
            

            </div>
            <div class="form-actions">
                <div class="btn-set pull-right">
                    <button type="submit" class="btn btn-lg green"  >Save</button>
                </div>
            </div>
        </div>
        <input type="reset" class="hide resetForm" >
        </form>
        <!-- END FORM-->
    </div>
</div>
@include('quizzes.templates')