<div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-globe"></i>Sign Ins
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
        </div>
    </div>
    <div class="portlet-body">
      <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="hide btn-group">
                    <a href="{{route('admin.sign_ins.init')}}" class="pjax-link" >
                        <button class="btn green" id="add_new" >

                            Add New <i class="fa fa-plus"></i>
                        </button>      
                      </a>                  
                    </div>
                </div>
                <div class="col-md-6"></div>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover table-dt" id="table-dt" >
            <thead>
                <tr class="tr-head">
                  <th valign="middle">Employee</th>
                  <th valign="middle">Date</th>
                  <th valign="middle">Sign in time</th>
                  <th valign="middle">Sign out time</th>
                    <th valign="middle">
                        Status</th>
                    <th valign="middle">From home </th>
                    <th valign="middle">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($sign_ins as $sign_in)
                <tr class="odd gradeX" id="data-row-{{$sign_in->id}}">
                  <td valign="middle">{{$sign_in->name}}</td>
                  <td valign="middle">
                  
                  {{ Carbon\Carbon::parse($sign_in->date)->format('l d, M Y') }}
                      
                  </td>
                  <td valign="middle">
                  
                  <?php if(isset($sign_in->sign_in_time)){?>
                  {{ Carbon\Carbon::parse($sign_in->sign_in_time)->format('l h:i:s A') }}
                  <?php ;}?>
                  </td>
                  <td valign="middle">
                  
                  <?php if(isset($sign_in->sign_out_time)){?>
                  {{ Carbon\Carbon::parse($sign_in->sign_out_time)->format('l h:i:s A') }}
                  <?php ;}?>
                  </td>
                  <td valign="middle">
                  {{$sign_in->status}}
                  </td>
                  <td valign="middle"><?php if($sign_in->from_home == 1){?>
                    Yes
                    <?php ;}else{?>
                    No
  <?php ;}?></td>
                  <td valign="middle">@if((Auth::user()->role_id == 2 || Auth::user()->role_id == 1) && $sign_in->status != "Leave")<a href="{{route('admin.sign_ins.edit',['id'=> $sign_in->id])}}" class="btn green pjax-link" ><i class="fa fa-edit"></i> Edit</a> @endif    </td>
                </tr>
                
                @endforeach

        </table>
    </div>
</div>