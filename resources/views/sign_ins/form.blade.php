<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i> {{ Carbon\Carbon::parse($sign_in->date)->format('l d, M Y') }}  </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form action="{{route('admin.sign_ins.save')}}" method="post" class="ajax_form">
        <input type="hidden" name="id" id="id" value="{{$sign_in->id}}" />
        <div  id="main-form" novalidate method="POST" class="larajsval form"> 
            <div class="form-body">
            <div class="mt-element-ribbon bg-grey-steel">
                                                <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                                                    <div class="ribbon-sub ribbon-clip"></div> Main Info </div>
                                                <p class="ribbon-content">
                                                <div class="form-group">
                                                @if(Auth::user()->role_id == 1 && $sign_in->emp_id == Null)
                                                <div class="form-group">
                    <label>Office</label>
                    <select name="office_id" data-validation="required" data-name="Office" class="form-control">
                    	<option value="">Select Office</option>
                        @foreach($offices as $office)
                    	<option value="{{$office->id}}" <?php if($office->id == $sign_in->office_id){?>selected<?php ;}?>>{{$office->name}}</option>
                        @endforeach
                    </select>
                    <span class="error_message"></span>
                </div>
                								@endif
                                                @if(Auth::user()->role_id == 2 || $sign_in->emp_id != Null)
                    							<input type="hidden" name="office_id" id="office_id" value="{{Auth::user()->office_id}}"  data-validation="" data-name="office_id"  />
                								@endif
                 @if($sign_in->emp_id == Null)
                <div class="form-group">
                    <label>Employee</label>
                    <select name="emp_id" data-validation="required" data-name="Employee" class="form-control <?php if($sign_in->emp_id == Null){?>options_dependent<?php ;}?>" data-depending-on="office_id">
                    	<option value="">Select Employee</option>
                        @foreach($employees as $employee)
                    	<option data-depending-value="<?php if($employee->office_id == 0){?>force_show<?php ;}else{?>{{$employee->office_id}}<?php ;}?>" value="{{$employee->id}}" <?php if($employee->id == $sign_in->emp_id){?>selected<?php ;}?>>{{$employee->name}}</option>
                        @endforeach
                    </select>
                    <span class="error_message"></span>
                </div>
                @endif
                
                @if($sign_in->emp_id != Null)
                <input type="hidden" name="emp_id" id="emp_id" value="{{$sign_in->emp_id}}"  data-validation="" data-name="emp_id"  />
                @endif
                
                <div class="form-group">
                    <label>User status</label>
                    <select name="user_status" data-validation="required" data-name="This Field" class="form-control">
                    	<option value="">Select Status</option>
                        
                    	<option  value="attended" <?php if($sign_in->status != "Leave" && $sign_in->status != "Weekend" && $sign_in->status != "Absent"){?> selected="selected"<?php ;}?>>Attended</option>
                    	<option  value="Leave" <?php if($sign_in->status == "Leave"){?> selected="selected"<?php ;}?>>Leave</option>
                    	<option  value="Weekend" <?php if($sign_in->status == "Weekend"){?> selected="selected"<?php ;}?>>Weekend</option>
                    </select>
                    <span class="error_message"></span>
                </div>
                <div class="dependent" data-depending-on="user_status" data-depending-value="attended">
                                                  <div class="form-group">
                    <label>Sign In Time</label>
                    <input name="sign_in_time" class="form-control dateTimePicker" data-validation="required" data-name="Name" value="{{$sign_in->sign_in_time}}"  >
                    <span class="error_message"></span>
                                                  
                                                  </div>
                <div class="form-group">
                    <label>Sign out time</label>
                    <input name="sign_out_time" type="text" data-validation="" data-name="Sign Out Time"  class="form-control dateTimePicker" value="{{$sign_in->sign_out_time}}"  >
                    <span class="error_message"></span>
                </div>
               
               <div class="form-group">
                    <label>From Home</label>
                    <select name="home" data-validation="required" data-name="This field" class="form-control" >
                    	<option value="">Select Option</option>
                    	<option  value="1" <?php if($sign_in->from_home == 1){?> selected="selected"<?php ;}?>>Yes</option>
                    	<option  value="0" <?php if($sign_in->from_home == 0){?> selected="selected"<?php ;}?>>No</option>
                    </select>
                    <span class="error_message"></span>
                </div>
                
                <div class="form-group">
                    <label>Day Type</label>
                    <select name="day_type" data-validation="required" data-name="This Field" class="form-control">
                    	<option value="">Select Day Type</option>
                        
                    	<option  value="normal" <?php if($sign_in->day_type == "normal"){?> selected="selected"<?php ;}?>>Normal</option>
                    	<option  value="1st_half" <?php if($sign_in->day_type == "1st_half"){?> selected="selected"<?php ;}?>>Half Day (First Half)</option>
                    	<option  value="2nd_half" <?php if($sign_in->day_type == "2nd_half"){?> selected="selected"<?php ;}?>>Half Day (Second Half)</option>
                    	<option  value="holiday" <?php if($sign_in->day_type == "holiday"){?> selected="selected"<?php ;}?>>Work On Holiday</option>
                    </select>
                    <span class="error_message"></span>
                </div>
                
                </div>
                
                
                
                
                
                
                
                
                
                </p>
                
                                            </div>
                                            
                                            </div>
                                            
            

            </div>
            <div class="form-actions">
                <div class="btn-set pull-right">
                    <button type="submit" class="btn btn-lg green"  >Save</button>
                </div>
            </div>
        </div>
        <input type="reset" class="hide resetForm" >
        </form>
        <!-- END FORM-->
    </div>
</div>
