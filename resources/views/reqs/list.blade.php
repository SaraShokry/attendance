<div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-globe"></i>{{$type}}
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
        </div>
    </div>
    <div class="portlet-body">
      <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                                    
                    </div>
                </div>
                <div class="col-md-6"></div>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover table-dt" id="table-dt" >
            <thead>
                <tr class="tr-head">
                  <th valign="middle">Employee</th>
                  <th valign="middle">Type</th>
                  <?php if($type == "Permissions"  || $type == "All" || $type == "Half Day" || $type == "Home" || $type == "Overtime"){?>
                  <th valign="middle">Day</th>
                  <?php ;}?>
                  <?php if($type == "Permissions"  || $type == "All" || $type == "Overtime" || $type == "Normal Leaves" || $type == "Emergency Leaves"){?>
                  <th valign="middle">From </th>
                  <th valign="middle">To </th>
                  <th valign="middle">Interval</th>
                  <?php ;}?>
                  
                  <?php if($type == "Half Day"  || $type == "All" || $type == "Home"){?>
                  <th valign="middle">
                  @if($type == "Half Day")
                  	Half
                    @endif
                    @if($type == "Home")
                  	Exchanged day
                    @endif
                    @if($type == "All")
                    	Notes
                    @endif
                  </th>
                  <?php ;}?>
                  <th valign="middle">
                      Requested at
                  </th>
                  <th valign="middle">Status</th>
                    <th valign="middle">
                        Action
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($reqs as $req)
                <tr class="odd gradeX" id="data-row-{{$req->id}}">
                  <td valign="middle">
                  {{$req->name}}
                      
                    </td>
                  <td valign="middle">
                  	<?php $req_type = str_replace("ves","ve",$req->type);
						$req_type= str_replace("ns","n",$req_type);
						echo str_replace("Home","Work From Home",$req_type);
					?>
                  </td>
                  <?php if($type == "Permissions"  || $type == "All" || $type == "Half Day"  || $type == "Home" || $type == "Overtime"){?>
                  <td valign="middle">{{$req->req_day}}</td>
                  <?php ;}?>
                  <?php if($type == "Permissions"  || $type == "All" || $type == "Overtime" || $type == "Normal Leaves" || $type == "Emergency Leaves"){?>
                  <td valign="middle">
                  <?php if($req->type == "Emergency Leaves" || $req->type == "Normal Leaves"){?>
                  <?php echo date("Y-m-d",strtotime($req->time_from));?>
                  <?php ;}else{?>
                  {{$req->time_from}}
                  <?php ;}?>
                  
                  </td>
                  <td valign="middle">
                  <?php if($req->type == "Emergency Leaves" || $req->type == "Normal Leaves"){?>
                  <?php echo date("Y-m-d",strtotime($req->time_to));?>
                  <?php ;}else{?>
                  {{$req->time_to}}
                  <?php ;}?>
                  
                  
                  </td>
                  <td valign="middle">
				  <?php if(isset($req->interval_in_days)){?>
                  {{$req->interval_in_days}} Day(s)
                  <?php ;}?>
                   <?php if(isset($req->interval_in_time)){?>
                  {{$req->interval_in_time}}
                  <?php ;}?>
				  <?php if($req->type == "Overtime" && $req->status == 2){?>
                  <form action="{{route('admin.reqs.edit_signout_interval')}}" method="post" class="ajax_form">
                  <input type="hidden" name="id" value="{{$req->id}}" />
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left" valign="middle"><input name="interval_in_time" class="form-control timepicker timepicker-24" data-validation="required" data-name="From" value="{{$req->interval_in_time}}" >
                  <span class="error_message"></span></td>
    <td width="50" align="left" valign="middle"><button type="submit" class="btn btn-lg green"  ><i class="fa fa-check"></i></button></td>
  </tr>
</table>

                  
                    
                  </form>
                  <?php ;}?>
                  
				  </td>
                  <?php ;}?>
                  
                  <?php if($type == "Half Day" || $type == "Home" || $type == "All"){?>
                  <td valign="middle">{{$req->add_info}}</td>
                  <?php ;}?>
                  <td valign="middle">
                      {{$req->created_at}}
                    </td>
                  <td valign="middle">
				  <?php $req_status = str_replace("1","Accepted", $req->status);
				  		$req_status = str_replace("2","Pending", $req_status);
						echo str_replace("0","Rejected", $req_status);
				  ?>
				  
				  </td>
                    <td valign="middle">
                    @if($req->status == 2 && $type != "All")
                        <a href="#" data-action="{{route('admin.reqs.accept',['id'=>$req->id])}}" class="btn green delete_single" ><i class="fa fa-check"></i> Accept</a> 
                        <a href="#" data-action="{{route('admin.reqs.reject',['id'=>$req->id])}}" class="btn red delete_single" ><i class="fa fa-remove"></i> Reject</a> 
                        @endif
                        @if(($req->status == 2 || $req->status == 1) && ($type == "All" && $req->taken == 0) )
                        <a href="#" data-action="{{route('admin.reqs.cancel',['id'=>$req->id])}}" class="btn red delete_single" ><i class="fa fa-remove"></i> Cancel Request</a> 
                        
                        @endif
                    </td>
                </tr>
                @endforeach
                

        </table>
    </div>
</div>