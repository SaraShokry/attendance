<?php 
	$i ="attendance_actions" ; $j = $type;
 ?>
@extends('admin.master')
@section('plugins_css')
@stop

@section('plugins_js')

@stop

@section('page_js')

@endSection
@section('layout_js')
<script type="text/javascript">

$(".timepicker-no-seconds").timepicker({
                autoclose: !0,
                minuteStep: <?php echo Auth::user()->getProfile->min_permission_unit;?>
            })
			$('.timepicker-no-seconds').keypress(function(event) {
       event.preventDefault();
       return false;
   });
   
</script>
@endSection
@section('add_inits')

@stop

@section('title')
Attendance Actions
@stop

@section('page_title')
Attendance Actions
@stop

@section('page_title_small')

@stop

@section('content')
@include($partialView)
@stop

