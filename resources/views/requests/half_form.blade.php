<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i> New Half Day Request </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form action="{{route('admin.requests.saveHalfDay')}}" method="post" class="ajax_form">
        <div  id="main-form" novalidate method="POST" class="larajsval form"> 
            <div class="form-body">
            <div class="mt-element-ribbon bg-grey-steel">
                                                <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                                                    <div class="ribbon-sub ribbon-clip"></div> Half Day Request </div>
                                                <p class="ribbon-content">
                                                <div class="form-group">
                                                  
                                                  <div class="form-group">
                                                                      

                </br>
                <div class="row">
                <div class="col-md-6">
                
                	Choose Day
                    <input name="half_day" class="form-control datePicker" data-validation="required" data-name="Day"   >
                    <span class="error_message"></span>
                    </div>
                    <div class="col-md-6">
                	Which Half Would You Need A Leave From?
                    <select id="half" name="half" data-validation="required" data-name="Half" class="form-control" >
                                            	
                                                <option value="First" >First Half</option>
                                                <option value="Second" >Second Half</option>
                                        </select> 
                    
                    <span class="error_message"></span>
                    </div>
                    </div>
                    
                    
                                                  </div>
                
                
                
                
                
                </p>
                
                                            </div>
                                            
                                            </div>
            

            </div>
            <div class="form-actions">
                <div class="btn-set pull-right">
                    <button type="submit" class="btn btn-lg green"  >Save</button>
                </div>
            </div>
        </div>
        <input type="reset" class="hide resetForm" >
        </form>
        <!-- END FORM-->
    </div>
</div>
