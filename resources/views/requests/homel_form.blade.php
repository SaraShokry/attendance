<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>  Work From Home  </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form action="{{route('admin.requests.saveNormal')}}" method="post" class="ajax_form">
        <div  id="main-form" novalidate method="POST" class="larajsval form"> 
            <div class="form-body">
            <div class="mt-element-ribbon bg-grey-steel">
                                                <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                                                    <div class="ribbon-sub ribbon-clip"></div> Normal Leave Request </div>
                                                <p class="ribbon-content">
                                                <div class="form-group">
                                                  
                                                  <div class="form-group">
                                                                      

                </br>
                <div class="row">
                <div class="col-md-6">
                
                	Date From
                    <input name="normal_from" class="form-control datePicker" data-validation="required" data-name="From"   >
                    <span class="error_message"></span>
                    </div>
                    <div class="col-md-6">
                	Date To
                    
                    <input name="normal_to" class="form-control datePicker" data-validation="required" data-name="To"   >
                    <span class="error_message"></span>
                    </div>
                    </div>
                    
                    
                                                  </div>
                
                
                
                
                
                </p>
                
                                            </div>
                                            
                                            </div>
            

            </div>
            <div class="form-actions">
                <div class="btn-set pull-right">
                    <button type="submit" class="btn btn-lg green"  >Save</button>
                </div>
            </div>
        </div>
        <input type="reset" class="hide resetForm" >
        </form>
        <!-- END FORM-->
    </div>
</div>
