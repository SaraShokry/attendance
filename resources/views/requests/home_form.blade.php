<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>  Work From Home Policy  </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body form">
    @if(Auth::user()->flexible_home == 1)
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="left" scope="col">Maximum Days Per Week</td>
            <td scope="col">&nbsp;</td>
        </tr>
          <tr>
            <td align="left">Work from system</td>
            <td>&nbsp;</td>
        </tr>
          <tr>
            <td align="left">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
          <tr>
            <td align="left">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
          <tr>
            <td align="left">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
      </table>
      @endif
      @if(Auth::user()->flexible_home == 0)
<table width="100%" border="0" cellspacing="0" cellpadding="20">
          <tr>
            <td align="center" scope="col">Your Default Work From Home Days Are: {{Auth::user()->home_days}}</td>
        </tr>
      </table>
@endif
    </div>
</div>
@if(Auth::user()->flexible_home == 1)
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>  Request Work From Home  </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form action="{{route('admin.requests.saveFlexHome')}}" method="post" class="ajax_form">
        <div  id="main-form" novalidate method="POST" class="larajsval form"> 
            <div class="form-body">
            <div class="mt-element-ribbon bg-grey-steel">
                                                <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                                                    <div class="ribbon-sub ribbon-clip"></div> Work from home Request </div>
                                                <p class="ribbon-content">
                                                <div class="form-group">
                                                  
                                                  <div class="form-group">
                                                                      

                </br>
                <div class="row">
                <div class="col-md-12">
                
                	Choose Day
                    <input name="home_day" class="form-control datePicker" data-validation="required" data-name="Day"   >
                    <span class="error_message"></span>
                    </div>
                    
                    </div>
                    
                    
                                                  </div>
                
                
                
                
                
                </p>
                
                                            </div>
                                            
                                            </div>
            

            </div>
            <div class="form-actions">
                <div class="btn-set pull-right">
                    <button type="submit" class="btn btn-lg green"  >Save</button>
                </div>
            </div>
        </div>
        <input type="reset" class="hide resetForm" >
        </form>
        <!-- END FORM-->
    </div>
</div>
@endif

@if(Auth::user()->flexible_home == 0 && Auth::user()->can_ex_days == 1)
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>  Request Work From Home  </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form action="{{route('admin.requests.saveExchangeHome')}}" method="post" class="ajax_form">
        <div  id="main-form" novalidate method="POST" class="larajsval form"> 
            <div class="form-body">
            <div class="mt-element-ribbon bg-grey-steel">
                                                <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                                                    <div class="ribbon-sub ribbon-clip"></div> Work from home Request </div>
                                                <p class="ribbon-content">
                                                <div class="form-group">
                                                  
                                                  <div class="form-group">
                                                                      

                </br>
                <div class="row">
                <div class="col-md-6">
                
                	I would like to work from home on
                    <input name="home_day" class="form-control datePicker" data-validation="required" data-name="Day"   >
                    <span class="error_message"></span>
                    </div>
                    <div class="col-md-6">
                
                	In exchange for
                    <select id="instead" name="instead" data-validation="required" data-name="This field" class="form-control" >
                                            	<?php 
														$default_home_days = explode(",",Auth::user()->home_days);
														
														foreach($default_home_days as $day){?>
                                                			<option value="{{$day}}" >{{$day}}</option>
                                                        <?php ;}?>
                                        </select>
                    <span class="error_message"></span>
                    </div>
                    
                    </div>
                    
                    
                                                  </div>
                
                
                
                
                
                </p>
                
                                            </div>
                                            
                                            </div>
            

            </div>
            <div class="form-actions">
                <div class="btn-set pull-right">
                    <button type="submit" class="btn btn-lg green"  >Save</button>
                </div>
            </div>
        </div>
        <input type="reset" class="hide resetForm" >
        </form>
        <!-- END FORM-->
    </div>
</div>
@endif
