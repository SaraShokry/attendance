<?php 
	$i ="employees" ; $j = "";
 ?>
@extends('admin.master')
@section('plugins_css')
@stop

@section('plugins_js')

@stop

@section('page_js')
<script type="text/javascript" src="{{asset('assets/admin/pages/scripts/employees.js')}}"></script>

@endSection

@section('add_inits')

@stop

@section('title')
Employees
@stop

@section('page_title')
Employees
@stop

@section('page_title_small')

@stop

@section('content')
@include($partialView)
@stop

