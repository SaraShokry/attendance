
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i> New  </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form action="{{route('admin.att_profiles.save')}}" method="post" class="ajax_form">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div  id="main-form" novalidate method="POST" class="larajsval form"> 
            <div class="form-body">
            <div class="mt-element-ribbon bg-grey-steel">
                                                <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                                                    <div class="ribbon-sub ribbon-clip"></div>General</div>
                                                <p class="ribbon-content">
                                                <div class="form-group">
                                                <div class="form-group">
                                                @if(Auth::user()->role_id == 1)
                                                <div class="form-group">
                    <label>Office</label>
                    <select name="office_id" data-validation="required" data-name="Office" class="form-control">
                    	<option value="">Select Office</option>
                        @foreach($offices as $office)
                    	<option value="{{$office->id}}" <?php if($office->id == $att_profile->office_id){?>selected<?php ;}?>>{{$office->name}}</option>
                        @endforeach
                    </select>
                    <span class="error_message"></span>
                </div>
                								@endif
                                                @if(Auth::user()->role_id == 2)
                    							<input type="hidden" name="office_id" id="office_id" value="{{Auth::user()->office_id}}"  data-validation="" data-name="office_id"  />
                								@endif
                                                <div class="form-group">
                    <label>Profile Type</label>
                    <select name="profile_type" data-validation="required" data-name="Profile Type" class="form-control">
                    	<option value="">Select Option</option>
                    	<option value="Normal" <?php if("Normal" == $att_profile->profile_type){?>selected<?php ;}?>>Normal</option>
                    	<option value="Holiday" <?php if("Holiday" == $att_profile->profile_type){?>selected<?php ;}?>>Work On Weekend/Holiday</option>
                    	<option value="1st_half" <?php if("1st_half" == $att_profile->profile_type){?>selected<?php ;}?>>Half Day(First Half)</option>
                    	<option value="2nd_half" <?php if("2nd_half" == $att_profile->profile_type){?>selected<?php ;}?>>Half Day(Second Half)</option>
                    </select>
                    <span class="error_message"></span>
                </div>
                                                 
                                                  <div class="form-group">
                    <label>Name</label>
                    <input type="hidden" name="company_id"  data-validation="" data-name="company_id" value="{{Auth::user()->company_id}}"/>
                    <input name="name" class="form-control" data-validation="required" data-name="Name" value="{{$att_profile->name}}" >
                    <span class="error_message"></span>
                    <input type="hidden" name="id" id="hiddenField" value="{{$att_profile->id}}" data-validation="" data-name="id" />
                                                  </div>
                                                  <div class="form-group">
                    <label>Sign In Required?</label>
                    <select name="sign_in_req" data-validation="required" data-name="Manager" class="form-control">
                    	<option value="">Select Option</option>
                    	<option value="1" <?php if(1 == $att_profile->sign_in_req){?>selected<?php ;}?>>Yes</option>
                    	<option value="0" <?php if(0 == $att_profile->sign_in_req){?>selected<?php ;}?>>No</option>
                    </select>
                    <span class="error_message"></span>
                </div>
                
                <div class="form-group dependent" data-depending-on="sign_in_req" data-depending-value="1">
                <label>Sign In Time Range</label></br>
                <div class="row">
                <div class="col-md-6">
                
                	From
                    <input name="sign_in_start_time" class="form-control timepicker timepicker-24" data-validation="required" data-name="From" value="{{$att_profile->sign_in_start_time}}" >
                    <span class="error_message"></span>
                    </div>
                    <div class="col-md-6">
                	To
                    
                    <input name="sign_in_end_time" class="form-control timepicker timepicker-24" data-validation="required" data-name="To" value="{{$att_profile->sign_in_end_time}}" >
                    <span class="error_message"></span>
                    </div>
                    </div>
                    
                    
                                                  </div>
                                                  <div class="form-group dependent" data-depending-on="sign_in_req" data-depending-value="1">
                    <label>Employees are Allowed To Sign In Before Sign In Start Time By The Following Time Interval </label>
                    <input name="time_allowed_before_sign_in" class="form-control timepicker timepicker-24" data-validation="required" data-name="This Field" value="{{$att_profile->time_allowed_before_sign_in}}" >
                    <span class="error_message"></span>
                                                  </div>
                <div class="form-group dependent" data-depending-on="sign_in_req" data-depending-value="1">
                    <label>Workday Hours</label>
                    <input name="work_hours" class="form-control timepicker timepicker-24" data-validation="required" data-name="Work Hours" value="{{$att_profile->work_hours}}" >
                    <span class="error_message"></span>
                                                  </div>
                                                  <div class="form-group dependent" data-depending-on="sign_in_req" data-depending-value="1">
                    <label>Employee must sign out by</label>
                    <input name="end_of_day" class="form-control timepicker timepicker-24" data-validation="required" data-name="End of day" value="{{$att_profile->end_of_day}}" >
                    <span class="error_message"></span>
                                                  </div>
                                                  <div class="form-group dependent" data-depending-on="sign_in_req" data-depending-value="1">
                    <label>Consider Employee Absent If Signed Out Before Calculated Sign Out Time By The Following Time Interval </label>
                    <input name="sign_out_cuttoff_time" class="form-control timepicker timepicker-24" data-validation="required" data-name="This Field" value="{{$att_profile->sign_out_cuttoff_time}}" >
                    <span class="error_message"></span>
                                                  </div>
                                                  
                                                  
                                                  
                                                  
                                                  
                                                  <div class="form-group dependent" data-depending-on= "profile_type" data-depending-value= "Normal">
  <label>Weekends</label>
  <select id="weekends" name="weekends[]" data-validation="required" data-name="Weekends" class="form-control select2-multiple select2" multiple>
                                            <optgroup label="Weekends">
                                            	
                                                <option value="Saturday" <?php  if (strpos($att_profile->weekends, 'Saturday') !== false) {?>selected="selected"<?php ;}?>>Saturday</option>
                                                <option value="Sunday" <?php  if (strpos($att_profile->weekends, 'Sunday') !== false) {?>selected="selected"<?php ;}?>>Sunday</option>
                                                <option value="Monday" <?php  if (strpos($att_profile->weekends, 'Monday') !== false) {?>selected="selected"<?php ;}?>>Monday</option>
                                                <option value="Tuesday" <?php  if (strpos($att_profile->weekends, 'Tuesday') !== false) {?>selected="selected"<?php ;}?>>Tuesday</option>
                                                <option value="Wednesday" <?php  if (strpos($att_profile->weekends, 'Wednesday') !== false) {?>selected="selected"<?php ;}?>>Wednesday</option>
                                                <option value="Thursday" <?php  if (strpos($att_profile->weekends, 'Thursday') !== false) {?>selected="selected"<?php ;}?>>Thursday</option>
                                                <option value="Friday" <?php  if (strpos($att_profile->weekends, 'Friday') !== false) {?>selected="selected"<?php ;}?>>Friday</option>
                                            </optgroup>
                                        </select>
  <span class="error_message"></span> </div>
  <div class="form-group  dependent" data-depending-on= "profile_type" data-depending-value= "Normal">
                    <label>Holiday Profile</label>
                    <select name="holiday_profile_id" data-validation="required" data-name="Holiday Profile" class="form-control options_dependent" data-depending-on="office_id">
                    	<option value="">Select Option</option>
                        @foreach($holiday_profiles as $holiday_profile)
                    	<option data-depending-value="{{$holiday_profile->office_id}}" value="{{$holiday_profile->id}}" <?php if($holiday_profile->id == $att_profile->holiday_profile_id){?>selected<?php ;}?>>{{$holiday_profile->name}}</option>
                        @endforeach
                    </select>
                    <span class="error_message"></span>
                </div>
                <div class="form-group  dependent" data-depending-on= "profile_type" data-depending-value= "Normal">
  <label>Week Start Day</label>
  <select id="week_start_day" name="week_start_day" data-validation="required" data-name="Week Start Day" class="form-control" >
                                            
                                            	
                                                <option value="1" <?php  if ($att_profile->week_start_day == 1)  {?>selected="selected"<?php ;}?>>Saturday</option>
                                                <option value="2" <?php  if ($att_profile->week_start_day == 2)  {?>selected="selected"<?php ;}?>>Sunday</option>
                                                <option value="3" <?php  if ($att_profile->week_start_day == 3)  {?>selected="selected"<?php ;}?>>Monday</option>
                                                <option value="4" <?php  if ($att_profile->week_start_day == 4)  {?>selected="selected"<?php ;}?>>Tuesday</option>
                                                <option value="5" <?php  if ($att_profile->week_start_day == 5)  {?>selected="selected"<?php ;}?>>Wednesday</option>
                                                <option value="6" <?php  if ($att_profile->week_start_day == 6)  {?>selected="selected"<?php ;}?>>Thursday</option>
                                                <option value="7" <?php  if ($att_profile->week_start_day == 7)  {?>selected="selected"<?php ;}?>>Friday</option>
                                            
                                        </select>
  <span class="error_message"></span> </div>
  				<div class="form-group dependent" data-depending-on="sign_in_req" data-depending-value="1">
  <label>Early Sign Out Action</label>
  <select id="early_signout_action" name="early_signout_action" data-validation="required" data-name="This field" class="form-control" >
                                            
                                            	
                                                <option value="Notify Managers" <?php  if ($att_profile->early_signout_action == "Notify Managers")  {?>selected="selected"<?php ;}?>>Notify Managers</option>
                                                <option value="No Notification" <?php  if ($att_profile->early_signout_action == "No Notification")  {?>selected="selected"<?php ;}?>>No Notification</option>
                                            
                                        </select>
  <span class="error_message"></span> </div>
  <div class="form-group dependent" data-depending-on="sign_in_req" data-depending-value="1">
                    <label>Early Sign Out Deduction (In Days)</label>
                    <input name="early_signout_deduction_days" class="form-control" data-validation="required,number" data-name="This field" value="{{$att_profile->early_signout_deduction_days}}" >
                    
                    <span class="error_message"></span>
                                                 
                
                </div>
                 <div class="form-group dependent" data-depending-on="sign_in_req" data-depending-value="1">
                    <label>Early Sign Out Deducted From</label>
                    <select name="early_signout_deduction_type" data-validation="required" data-name="This field" class="form-control">
                    	<option value="">Select Option</option>
                    	<option value="Leaves" <?php if("Leaves" == $att_profile->early_signout_deduction_type){?>selected<?php ;}?>>Normal Leaves</option>
                    	<option value="Salary" <?php if("Salary" == $att_profile->early_signout_deduction_type){?>selected<?php ;}?>>Salary</option>
                    </select>
                    <span class="error_message"></span>
                </div>
                
                
                                                  
                                                  
                                                  
                                                  
                </p>
                
                                            </div>
                                            
                                            </div>
                                            </div>
            <div class="mt-element-ribbon bg-grey-steel">
                                                <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                                                    <div class="ribbon-sub ribbon-clip"></div> Leaves </div>
                                                <p class="ribbon-content">
                                                <div class="dependent" data-depending-on= "profile_type" data-depending-value= "Normal">
                                                <div class="form-group">
  <label>Number Of Normal Leaves Per Year</label>
  <input name="allowed_normal_leaves" class="form-control" data-validation="required,number" data-name="Normal Leave Per Year" value="{{$att_profile->allowed_normal_leaves}}" >
  <span class="error_message"></span> </div>
                <div class="form-group">
  <label>Normal Leave Cutt-Off Time</label>
  <input name="normal_leave_cuttoff" class="form-control timepicker timepicker-24" data-validation="required" data-name="Cuttoff Time" value="{{$att_profile->normal_leave_cuttoff}}" >
  <span class="error_message"></span> </div>
  <div class="form-group">
                    <label>Allow Half Day?</label>
                    <select name="allow_half_day" data-validation="required" data-name="This field" class="form-control">
                    	<option value="">Select Option</option>
                    	<option value="1" <?php if(1 == $att_profile->allow_half_day){?>selected<?php ;}?>>Yes</option>
                    	<option value="0" <?php if(0 == $att_profile->allow_half_day){?>selected<?php ;}?>>No</option>
                    </select>
                    <span class="error_message"></span>
                </div>
                <div class="form-group dependent" data-depending-on="allow_half_day" data-depending-value="1">
                    <label>First Half Profile</label>
                    <select name="first_half_day_profile_id" data-validation="required" data-name="This field" class="form-control options_dependent" data-depending-on="office_id">
                    	<option value="">Select Option</option>
                        @foreach($half_profiles as $profile)
                    	<option  data-depending-value="{{$profile->office_id}}" value="{{$profile->id}}" <?php if($profile->id == $att_profile->first_half_day_profile_id){?>selected<?php ;}?>>{{$profile->name}}</option>
                        @endforeach
                    </select>
                    <span class="error_message"></span>
                </div>
                <div class="form-group dependent" data-depending-on="allow_half_day" data-depending-value="1">
                    <label>Second Half Profile</label>
                    <select name="second_half_day_profile_id" data-validation="required" data-name="This field" class="form-control options_dependent" data-depending-on="office_id">
                    	<option value="">Select Option</option>
                        @foreach($half_profiles as $profile)
                    	<option data-depending-value="{{$profile->office_id}}" value="{{$profile->id}}" <?php if($profile->id == $att_profile->second_half_day_profile_id){?>selected<?php ;}?>>{{$profile->name}}</option>
                        @endforeach
                    </select>
                    <span class="error_message"></span>
                </div>
                <div class="form-group">
  <label>Number Of Emergency Leaves Per Year</label>
  <input name="allowed_emergency_leaves" class="form-control" data-validation="required,number" data-name="Emergency Leave Per Year" value="{{$att_profile->allowed_emergency_leaves}}" >
  <span class="error_message"></span> </div>
												<div class="form-group">
                    <label>Allow Emergency Leaves Without Manager Permission?</label>
                    <select name="emergency_leave_without_permission" data-validation="required" data-name="This field" class="form-control">
                    	<option value="">Select Option</option>
                    	<option value="1" <?php if(1 == $att_profile->emergency_leave_without_permission){?>selected<?php ;}?>>Yes</option>
                    	<option value="0" <?php if(0 == $att_profile->emergency_leave_without_permission){?>selected<?php ;}?>>No</option>
                    </select>
                    <span class="error_message"></span>
                </div>  
                </div>
  
  <div class="form-group">
                    <label>No Show Absence Deduction/Deduction After Exhausting Emergency Leaves (In Days)</label>
                    <input name="emergency_penalty" class="form-control" data-validation="required,number" data-name="This field" value="{{$att_profile->emergency_penalty}}" >
                    
                    <span class="error_message"></span>
                                                  </div>
                    <div class="form-group">
                    <label>Extra Absence Deducted From</label>
                    <select name="emergency_penalty_type" data-validation="required" data-name="This field" class="form-control">
                    	<option value="">Select Option</option>
                    	<option value="Leaves" <?php if("Leaves" == $att_profile->emergency_penalty_type){?>selected<?php ;}?>>Normal Leaves</option>
                    	<option value="Salary" <?php if("Salary" == $att_profile->emergency_penalty_type){?>selected<?php ;}?>>Salary</option>
                    </select>
                    <span class="error_message"></span>
                </div>
                                                </p>
                                            
                                            </div>  
            <div class="mt-element-ribbon bg-grey-steel dependent" data-depending-on="sign_in_req" data-depending-value="1">
                                                <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                                                    <div class="ribbon-sub ribbon-clip"></div> Overtime Work Policy </div>
                                                <p class="ribbon-content"><div class="form-group">
                    <label>Allow Overtime</label>
                    <select name="overtime_permissible" data-validation="required" data-name="This field" class="form-control">
                    	<option value="">Select Option</option>
                    	<option value="1" <?php if(1 == $att_profile->overtime_permissible){?>selected<?php ;}?>>Yes</option>
                    	<option value="0" <?php if(0 == $att_profile->overtime_permissible){?>selected<?php ;}?>>No</option>
                    </select>
                    <span class="error_message"></span>
                </div>
                <div class="dependent" data-depending-on="overtime_permissible" data-depending-value="1">
                <div class="form-group">
                    <label>Do Not Consider Overtime Unless After The Calculated Sign Out Time By The Following Time Interval </label>
                    <input name="min_overtime_cuttoff" class="form-control timepicker timepicker-24" data-validation="required" data-name="This Field" value="{{$att_profile->min_overtime_cuttoff}}" >
                    <span class="error_message"></span>
                                                  </div>
                <div class="form-group">
                    <label>Apply Overtime Limit</label>
                    <select name="overtime_limit" data-validation="required" data-name="This field" class="form-control">
                    	<option value="">Select Option</option>
                    	<option value="1" <?php if(1 == $att_profile->overtime_limit){?>selected<?php ;}?>>Yes</option>
                    	<option value="0" <?php if(0 == $att_profile->overtime_limit){?>selected<?php ;}?>>No</option>
                    </select>
                    <span class="error_message"></span>
                </div>
                <div class="dependent" data-depending-on="overtime_limit" data-depending-value="1">
                <div class="form-group dependent" data-depending-on="profile_type" data-depending-value="Normal">
                    <label>Overtime Hours Per Month Limit</label>
                    <input name="max_overtime_per_month" class="form-control timepicker timepicker-24" data-validation="required" data-name="This field" value="{{$att_profile->max_overtime_per_month}}" >
                    <span class="error_message"></span>
                                                  </div>
                                                  <div class="form-group">
                    <label>Overtime Hours Per Day Limit</label>
                    <input name="max_overtime_per_day" class="form-control" data-validation="required" data-name="This field" value="{{$att_profile->max_overtime_per_day}}" >
                    <span class="error_message"></span>
                    </div>
                                                  </div>
                                                  <div class="form-group">
                    <label>Overtime Hour Factor</label>
                    <input name="overtime_payment_factor" class="form-control" data-validation="required,number" data-name="This field" value="{{$att_profile->overtime_payment_factor}}" >
                    <span class="error_message"></span>
                                                  </div>
                                                  </div>
                                                 
                                                </p>
                                            
                                            </div> 
                                            <div class="dependent" data-depending-on="sign_in_req" data-depending-value="1">
                                            <div class="mt-element-ribbon bg-grey-steel dependent" data-depending-on="sign_in_req" data-depending-value="1">
                                                <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                                                    <div class="ribbon-sub ribbon-clip"></div> Tardiness Policy </div>
                                                <p class="ribbon-content">
                
                <div class="form-group">
                    <label>Do not consider late unless signed in after max sign in time by the following interval </label>
                    <input name="max_lateness_permissibility" class="form-control timepicker timepicker-24" data-validation="required" data-name="This Field" value="{{$att_profile->max_lateness_permissibility}}" >
                    <span class="error_message"></span>
                                                  </div>
                                                  <div class="form-group">
                    <label>Consider employee as tardy (minor) if signed in later than the maximum sign in time by up to :</label>
                    <input name="minor_tardiness_range" class="form-control timepicker timepicker-24" data-validation="required" data-name="This Field" value="{{$att_profile->minor_tardiness_range}}" >
                    <span class="error_message"></span>
                                                  </div>
                                                  <div class="form-group">
                    <label>Tardiness (minor) deduction (In Days)</label>
                    <input name="minor_tardiness_penalty" class="form-control" data-validation="required,number" data-name="This field" value="{{$att_profile->minor_tardiness_penalty}}" >
                    
                    <span class="error_message"></span>
                                                  </div>
                    							  <div class="form-group">
                    <label>Tardiness (minor)e Deducted From</label>
                    <select name="minor_tardiness_penalty_type" data-validation="required" data-name="This field" class="form-control">
                    	<option value="">Select Option</option>
                    	<option value="Leaves" <?php if("Leaves" == $att_profile->minor_tardiness_penalty_type){?>selected<?php ;}?>>Normal Leaves</option>
                    	<option value="Salary" <?php if("Salary" == $att_profile->minor_tardiness_penalty_type){?>selected<?php ;}?>>Salary</option>
                    </select>
                    <span class="error_message"></span>
                </div>
                                                  <div class="form-group">
                    <label>Consider employee as tardy (major) if signed in later than the maximum sign in time by up to :</label>
                    <input name="major_tardiness_range" class="form-control timepicker timepicker-24" data-validation="required" data-name="This Field" value="{{$att_profile->major_tardiness_range}}" >
                    <span class="error_message"></span>
                                                  </div>
                <div class="form-group">
                    <label>Tardiness (major) deduction (In Days)</label>
                    <input name="major_tardiness_penalty" class="form-control" data-validation="required,number" data-name="This field" value="{{$att_profile->major_tardiness_penalty}}" >
                    
                    <span class="error_message"></span>
                                                  </div>
                    							  <div class="form-group">
                    <label>Tardiness (major)e Deducted From</label>
                    <select name="major_tardiness_penalty_type" data-validation="required" data-name="This field" class="form-control">
                    	<option value="">Select Option</option>
                    	<option value="Leaves" <?php if("Leaves" == $att_profile->major_tardiness_penalty_type){?>selected<?php ;}?>>Normal Leaves</option>
                    	<option value="Salary" <?php if("Salary" == $att_profile->major_tardiness_penalty_type){?>selected<?php ;}?>>Salary</option>
                    </select>
                    <span class="error_message"></span>
                </div>
                
                
                                                  
                  
                                                  
                                                  
                                                  
                                                 
                                                </p>
                                            
                                            </div>
                                            </div>
                                             <div class="dependent" data-depending-on="profile_type" data-depending-value="Normal">
                                            <div class="mt-element-ribbon bg-grey-steel">
                                                <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                                                    <div class="ribbon-sub ribbon-clip"></div> Holiday/Weekend Work Policy </div>
                                                <p class="ribbon-content"> <div class="form-group">
                    <label>Can Be Assigned Work On Holidays/Weekends</label>
                    <select name="can_work_holiday" data-validation="required" data-name="This field" class="form-control">
                    	<option value="">Select Option</option>
                    	<option value="1" <?php if(1 == $att_profile->can_work_holiday){?>selected<?php ;}?>>Yes</option>
                    	<option value="0" <?php if(0 == $att_profile->can_work_holiday){?>selected<?php ;}?>>No</option>
                    </select>
                    <span class="error_message"></span>
                </div>
                <div class="dependent" data-depending-on="can_work_holiday" data-depending-value="1">
                <div class="form-group">
                    <label>Work On Holiday Profile</label>
                    <select name="work_on_holiday_profile_id" data-validation="required" data-name="This field" class="form-control">
                    	<option value="">Select Option</option>
                        @foreach($work_on_holiday_profiles as $profile)
                    	<option  value="{{$profile->id}}" <?php if($profile->id == $att_profile->work_on_holiday_profile_id){?>selected<?php ;}?>>{{$profile->name}}</option>
                        @endforeach
                    </select>
                    <span class="error_message"></span>
                </div>
               									  <div class="form-group">
                    <label>Holiday/Weekend Day Factor</label>
                    <input name="holiday_factor" class="form-control" data-validation="required,number" data-name="This field" value="{{$att_profile->holiday_factor}}" >
                    <span class="error_message"></span>
                                                  </div>
                                                  <div class="form-group">
                    <label>Holiday/Weekend Extra Hour Factor</label>
                    <input name="extra_holiday_hour_factor" class="form-control" data-validation="required,number" data-name="This field" value="{{$att_profile->extra_holiday_hour_factor}}" >
                    <span class="error_message"></span>
                                                  </div>
                                                  </div>
                                                </p>
                                            
                                            </div> 
                                                                                                     
            <div class="mt-element-ribbon bg-grey-steel">
                                                <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                                                    <div class="ribbon-sub ribbon-clip"></div> Permissions </div>
                                                <p class="ribbon-content"><div class="form-group">
                    <label>Minimum Permission Unit</label>
                    <input name="min_permission_unit" class="form-control timepicker timepicker-24" data-validation="required" data-name="This Field" value="{{$att_profile->min_permission_unit}}" >
                    <span class="error_message"></span>
                                                  </div>
                                                  <div class="form-group">
                    <label>Maximum Permissions Units Per Day</label>
                    <input name="max_units_per_day" class="form-control" data-validation="required,number" data-name="This Field" value="{{$att_profile->max_units_per_day}}" >
                    <span class="error_message"></span>
                                                  </div>
                                                  <div class="form-group">
                    <label>Maximum Permissions Units Per month</label>
                    <input name="max_units_per_month" class="form-control" data-validation="required,number" data-name="This Field" value="{{$att_profile->max_units_per_month}}" >
                    <span class="error_message"></span>
                                                  </div>
                                                </p>
                                            
                                            </div>      
                                            </div>                                                        
             
                                                                                                  

            </div>
            <div class="form-actions">
                <div class="btn-set pull-right">
                    <input type="submit" name="update" class="<?php if(isset($_GET['clone'])){?>hide<?php ;}?> btn btn-lg green do_save" value="Save"  >
                    <input type="submit" name="new" class="<?php if(!isset($_GET['clone'])){?>hide<?php ;}?> btn btn-lg blue do_clone" value="Save as new"  >
                </div>
            </div>
        </div>
        <input type="reset" class="hide resetForm" >
        </form>
        <!-- END FORM-->
    </div>
</div>
