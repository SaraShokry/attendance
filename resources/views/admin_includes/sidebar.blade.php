<div class="page-sidebar-wrapper">
		<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
		<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
		<div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                        <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
                      <li class="sidebar-toggler-wrapper">
                            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                            <div class="sidebar-toggler">
                                <span></span>
                            </div>
                            <!-- END SIDEBAR TOGGLER BUTTON -->
                        </li>
                        <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
                        <li class="sidebar-search-wrapper">
                            <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
                            <!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
                            <!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
                            <form class="sidebar-search hide" action="page_general_search_3.html" method="POST">
                                <a href="javascript:;" class="remove">
                                    <i class="icon-close"></i>
                                </a>
                                <div class="input-group">
                                    <input class="form-control" placeholder="Search..." type="text">
                                    <span class="input-group-btn">
                                        <a href="javascript:;" class="btn submit">
                                            <i class="icon-magnifier"></i>
                                        </a>
                                    </span>
                                </div>
                            </form>
                            <!-- END RESPONSIVE QUICK SEARCH FORM -->
                        </li>
                        <h3></h3>
                        @if(Auth::user()->user_type == "Admin")
                       <li class="nav-item start <?php if($i=='companies'){?>active<?php ;}?>">
                            <a href="/admin/companies" class="nav-link nav-toggle">
                                <i class="fa fa-folder" aria-hidden="true"></i>
                                <span class="title">Companies</span>
                            </a>
                            
                        </li>
                       <li class="nav-item start <?php if($i=='partners'){?>active<?php ;}?>">
                            <a href="/admin/partners" class="nav-link nav-toggle">
                                <i class="fa fa-user" aria-hidden="true"></i>
                                <span class="title">Partners</span>
                            </a>
                            
                        </li>
                        
                        @endif
                        
                        @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 2)
                        @if(Auth::user()->role_id == 1)
                        <li class="nav-item start <?php if($i=='offices'){?>active<?php ;}?>">
                            <a href="/admin/offices" class="nav-link nav-toggle">
                                <i class="fa fa-globe" aria-hidden="true"></i>
                                <span class="title">Offices</span>
                            </a>
                            
                        </li>
                        @endif
                        <li class="nav-item start <?php if($i=='holidays'){?>active<?php ;}?>">
                            <a href="#" class="nav-link nav-toggle">
                                <i class="fa fa-hotel"></i>
                                <span class="title">Holidays</span>
                            </a>
                           
                            <ul class="sub-menu">
                                <li class="nav-item <?php if($i=='holidays' && $j =='holidays'){?>active open<?php ;}?>">
                                    <a href="/admin/holidays" class="nav-link ">
                                    	<i class="fa fa-hotel"></i>
                                        <span class="title">Holidays</span>
                                    </a>
                                </li>
                                
                                <li class="nav-item <?php if($i=='holidays' && $j =='holiday_profiles'){?>active open<?php ;}?>">
                                    <a href="/admin/holiday_profiles" class="nav-link ">
                                    	<i class="fa fa-folder"></i>
                                        <span class="title">Holiday Profiles</span>
                                    </a>
                                </li>
                            </ul>
                            
                            
                            
                        </li>
                        <li class="nav-item start <?php if($i=='att_profiles'){?>active<?php ;}?>">
                            <a href="/admin/att_profiles" class="nav-link nav-toggle">
                                <i class="fa fa-folder" aria-hidden="true"></i>
                                <span class="title">Attendance Profiles</span>
                            </a>
                            
                        </li>
                        <li class="nav-item start <?php if($i=='balance'){?>active<?php ;}?>">
                            <a href="/admin/balance" class="nav-link nav-toggle">
                                <i class="fa fa-balance-scale" aria-hidden="true"></i>
                                <span class="title">Balance</span>
                            </a>
                            
                        </li>
                        <li class="nav-item start <?php if($i=='sign_ins'){?>active<?php ;}?>">
                            <a href="/admin/sign_ins" class="nav-link nav-toggle">
                                <i class="fa fa-sign-in" aria-hidden="true"></i>
                                <span class="title">Edit Attendance</span>
                            </a>
                            
                        </li>
                        <li class="nav-item start <?php if($i=='departments'){?>active<?php ;}?>">
                            <a href="/admin/departments" class="nav-link nav-toggle">
                                <i class="fa fa-cubes" aria-hidden="true"></i>
                                <span class="title">Departments</span>
                            </a>
                            
                        </li>
                        <li class="nav-item start <?php if($i=='positions'){?>active<?php ;}?>">
                            <a href="/admin/positions" class="nav-link nav-toggle">
                                <i class="fa fa-users" aria-hidden="true"></i>
                                <span class="title">Positions</span>
                            </a>
                            
                        </li>
                        
                        
                        <li class="nav-item start <?php if($i=='employees'){?>active<?php ;}?>">
                            <a href="/admin/employees" class="nav-link nav-toggle">
                                <i class="fa fa-user" aria-hidden="true"></i>
                                <span class="title">Employees</span>
                            </a>
                            
                        </li>
                       
                      
                        @endif
                        @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 2|| Auth::user()->role_id == 3 || Auth::user()->role_id == 4)
                        @if(Auth::user()->role_id != 1)
                        <li class="nav-item start <?php if($i=='attendance_actions'){?>active<?php ;}?>">
                            <a href="#" class="nav-link nav-toggle">
                                <i class="fa fa-user"></i>
                                <span class="title">Attendance Actions</span>
                            </a>
                            
                            <ul class="sub-menu">
                            	
                                <li class="nav-item <?php if($i=='attendance_actions' && $j =='Permissions'){?>active open<?php ;}?>">
                                    <a href="/admin/requests/Permissions" class="nav-link ">
                                    	<i class="fa fa-clock-o"></i>
                                        <span class="title">Permissions</span>
                                    </a>
                                </li>
                                <li class="nav-item <?php if($i=='attendance_actions' && $j =='Normal Leaves'){?>active open<?php ;}?>">
                                    <a href="/admin/requests/Normal Leaves" class="nav-link ">
                                    	<i class="fa fa-question-circle"></i>
                                        <span class="title">Normal Leaves</span>
                                    </a>
                                </li>
                                <li class="nav-item <?php if($i=='attendance_actions' && $j =='Half Day'){?>active open<?php ;}?>">
                                    <a href="/admin/requests/Half Day" class="nav-link ">
                                    	<i class="fa fa-clock-o"></i>
                                        <span class="title">Half Day</span>
                                    </a>
                                </li>
                                <li class="nav-item <?php if($i=='attendance_actions' && $j =='Emergency Leaves'){?>active open<?php ;}?>">
                                    <a href="/admin/requests/Emergency Leaves" class="nav-link ">
                                    	<i class="fa fa-ambulance"></i>
                                        <span class="title">Emergency Leaves</span>
                                    </a>
                                </li>
                                @if(Auth::user()->can_work_home == 1)
                                <li class="nav-item <?php if($i=='attendance_actions' && $j =='Home'){?>active open<?php ;}?>">
                                    <a href="/admin/requests/Home" class="nav-link ">
                                    	<i class="fa fa-home"></i>
                                        <span class="title">Work From Home</span>
                                    </a>
                                </li>
                                @endif
                            </ul>
                            
                            
                        </li>
                        <li class="nav-item start <?php if($i=='user_requests'){?>active<?php ;}?>">
                            <a href="#" class="nav-link nav-toggle">
                                <i class="fa fa-question"></i>
                                <span class="title">Your Requests </span>
                            </a>
                            
                            <ul class="sub-menu">
                            	
                                <li class="nav-item <?php if($i=='user_requests' && $j ==2){?>active open<?php ;}?>">
                                    <a href="/admin/reqs/user_requests/{{Auth::user()->id}}/2/0" class="nav-link ">
                                    	<i class="fa fa-clock-o"></i>
                                        <span class="title">Pending</span>
                                    </a>
                                </li>
                                <li class="nav-item <?php if($i=='user_requests' && $j ==1){?>active open<?php ;}?>">
                                    <a href="/admin/reqs/user_requests/{{Auth::user()->id}}/1/0" class="nav-link ">
                                    	<i class="fa fa-check"></i>
                                        <span class="title">Accepted</span>
                                    </a>
                                </li>
                                <li class="nav-item <?php if($i=='user_requests' && $j ==0){?>active open<?php ;}?>">
                                    <a href="/admin/reqs/user_requests/{{Auth::user()->id}}/0/0" class="nav-link ">
                                    	<i class="fa fa-remove"></i>
                                        <span class="title">Rejected</span>
                                    </a>
                                </li>
                                <li class="nav-item <?php if($i=='user_requests' && $j ==3){?>active open<?php ;}?>">
                                    <a href="/admin/reqs/user_requests/{{Auth::user()->id}}/1/1" class="nav-link ">
                                    	<i class="fa fa-check"></i>
                                        <span class="title">Completed</span>
                                    </a>
                                </li>
                                
                            </ul>
                            
                            
                        </li>
                        
                        <li class="nav-item start <?php if($i=='user_reports'){?>active<?php ;}?>">
                            <a href="#" class="nav-link nav-toggle">
                                <i class="fa fa-line-chart"></i>
                                <span class="title">Your Reports</span>
                            </a>
                            
                            <ul class="sub-menu">
                            @if(Auth::user()->getProfile->sign_in_req ==1)
                                <li class="nav-item <?php if($i=='user_reports' && $j =='attendance'){?>active open<?php ;}?>">
                                    <a href="/admin/user_reports/attendance" class="nav-link ">
                                    	<i class="fa fa-user"></i>
                                        <span class="title">Attendance</span>
                                    </a>
                                </li>
                              @endif  
                                <li class="nav-item <?php if($i=='user_reports' && $j =='penalties'){?>active open<?php ;}?>">
                                    <a href="/admin/user_reports/penalties" class="nav-link">
                                    	<i class="fa fa-minus-circle"></i>
                                        <span class="title">Penalties</span>
                                    </a>
                                </li>
                            </ul>
                            
                            
                        </li>
                        
                        @endif
                        
                        @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 2 || Auth::user()->role_id == 3)
                        <li class="nav-item start <?php if($i=='Overtime_report'){?>active<?php ;}?>">
                            <a href="/admin/reqs/Overtime_report" class="nav-link nav-toggle">
                                <i class="fa fa-clock-o" aria-hidden="true"></i>
                                <span class="title">Overtime Report</span>
                            </a>
                            
                        </li>
                        <li class="nav-item start <?php if($i=='no_sign_outs'){?>active<?php ;}?>">
                            <a href="/admin/sign_ins/no_sign_outs" class="nav-link nav-toggle">
                                <i class="fa fa-sign-in" aria-hidden="true"></i>
                                <span class="title">No Sign Out</span>
                            </a>
                            
                        </li>
                        <li class="nav-item start <?php if($i=='inbound_rquests'){?>active<?php ;}?>">
                            <a href="#" class="nav-link nav-toggle">
                                <i class="fa fa-question"></i>
                                <span class="title">Inbound Requests</span>
                            </a>
                            
                            <ul class="sub-menu">
                            	
                                <li class="nav-item <?php if($i=='inbound_rquests' && $j =='Permissions'){?>active open<?php ;}?>">
                                    <a href="/admin/reqs/Permissions" class="nav-link ">
                                    	<i class="fa fa-clock-o"></i>
                                        <span class="title">Permissions</span>
                                    </a>
                                </li>
                                <li class="nav-item <?php if($i=='inbound_rquests' && $j =='Normal Leaves'){?>active open<?php ;}?>">
                                    <a href="/admin/reqs/Normal Leaves" class="nav-link ">
                                    	<i class="fa fa-question-circle"></i>
                                        <span class="title">Normal Leaves</span>
                                    </a>
                                </li>
                                <li class="nav-item <?php if($i=='inbound_rquests' && $j =='Half Day'){?>active open<?php ;}?>">
                                    <a href="/admin/reqs/Half Day" class="nav-link ">
                                    	<i class="fa fa-clock-o"></i>
                                        <span class="title">Half Day</span>
                                    </a>
                                </li>
                                <li class="nav-item <?php if($i=='inbound_rquests' && $j =='Emergency Leaves'){?>active open<?php ;}?>">
                                    <a href="/admin/reqs/Emergency Leaves" class="nav-link ">
                                    	<i class="fa fa-ambulance"></i>
                                        <span class="title">Emergency Leaves</span>
                                    </a>
                                </li>
                                <li class="nav-item <?php if($i=='inbound_rquests' && $j =='Overtime'){?>active open<?php ;}?>">
                                    <a href="/admin/reqs/Overtime" class="nav-link ">
                                    	<i class="fa fa-ambulance"></i>
                                        <span class="title">Overtime Approvals</span>
                                    </a>
                                </li>
                                
                                <li class="nav-item <?php if($i=='inbound_rquests' && $j =='Home'){?>active open<?php ;}?>">
                                    <a href="/admin/reqs/Home" class="nav-link ">
                                    	<i class="fa fa-home"></i>
                                        <span class="title">Work From Home</span>
                                    </a>
                                </li>
                                <li class="nav-item <?php if($i=='inbound_rquests' && $j =='Alls'){?>active open<?php ;}?>">
                                    <a href="/admin/reqs/Alls" class="nav-link ">
                                    	<i class="fa fa-calendar"></i>
                                        <span class="title">History</span>
                                    </a>
                                </li>
                                
                            </ul>
                            
                            
                        </li>
                        @endif
                        
                        @endif
                        
                        
                        
                        
                        
                    </ul>
                    <!-- END SIDEBAR MENU -->
                    <!-- END SIDEBAR MENU -->
                </div>
	</div>