<div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-globe"></i>Materials
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <button class="btn green" id="add_new" data-action="{{route('admin.material.init')}}"
>
                            Add New <i class="fa fa-plus"></i>
                        </button>                        
                    </div>
                </div>
                <div class="col-md-6"></div>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover table-dt" id="table-dt" >
            <thead>
                <tr class="tr-head">
                    <th valign="middle">
                        Title
                    </th>
                    <th valign="middle">Course</th>
                    <th valign="middle">Type</th>
                    <th valign="middle">File Type</th>
                    <th valign="middle">Order</th>
                    <th valign="middle">
                        Created at
                    </th>
                    <th valign="middle">
                        Action
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($materials as $material)
                <tr class="odd gradeX" id="data-row-{{$material->id}}">
                    <td valign="middle">
                        {{$material->name}}
                    </td>
                    <td valign="middle"><?php if(isset($material->getCourse->name)){?>{{$material->getCourse->name}}<?php ;}?></td>
                    <td valign="middle">{{$material->material_type}} Material</td>
                    <td valign="middle">{{$material->type}}</td>
                    <td valign="middle">{{$material->material_order}}</td>
                    <td valign="middle">
                        {{$material->created_at}}
                    </td>
                    <td valign="middle">
                        <a href="{{route('admin.material.edit',['id'=>$material->id])}}" class="btn green pjax-link" ><i class="fa fa-edit"></i> Edit</a> 
                        <a href="{{route('admin.material.delete',['id'=>$material->id])}}" class="btn red remove-course" ><i class="fa fa-remove"></i> Delete</a> 
                    </td>
                </tr>
                @endforeach
            </tbody>
                        
        </table>
    </div>
</div>