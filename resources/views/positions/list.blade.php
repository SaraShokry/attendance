<div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-globe"></i>Positions
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
        </div>
    </div>
    <div class="portlet-body">
      <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                    <a href="{{route('admin.positions.init')}}" class="pjax-link" >
                        <button class="btn green" id="add_new" data-action="{{route('admin.positions.init')}}">

                            Add New <i class="fa fa-plus"></i>
                        </button>      
                        </a>                  
                    </div>
                </div>
                <div class="col-md-6"></div>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover table-dt" id="table-dt" >
            <thead>
                <tr class="tr-head">
                  <th valign="middle">
                      Name
                  </th>
                  <th valign="middle">Office</th>
                    <th valign="middle">
                        Created at
                    </th>
                    <th valign="middle">
                        Action
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($positions as $position)
                <tr class="odd gradeX" id="data-row-{{$position->id}}">
                  <td valign="middle">
                      {{$position->name}}
                    </td>
                  <td valign="middle">{{$position->getOffice->name}}</td>
                    <td valign="middle">
                        {{$position->created_at}}
                    </td>
                    <td valign="middle">
                        <a href="{{route('admin.positions.edit',['id'=>$position->id])}}" class="btn green pjax-link" ><i class="fa fa-edit"></i> Edit</a> 
                        <a href="#" data-action="{{route('admin.positions.delete',['id'=>$position->id])}}"  class="btn red delete_single" ><i class="fa fa-remove"></i> Delete</a> 
                    </td>
                </tr>
                @endforeach

        </table>
    </div>
</div>