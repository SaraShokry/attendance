<div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption capitalize">
            <i class="fa fa-globe"></i>{{$type}}
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
        </div>
    </div>
    <div class="portlet-body">
      <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                                      
                  </div>
                </div>
                <div class="col-md-6"></div>
            </div>
        </div>
        @if($type == "attendance")
      <table class="table table-striped table-bordered table-hover table-dt" id="table-dt" >
            <thead>
                <tr class="tr-head">
                  <th valign="middle">Date</th>
                  <th valign="middle">Sign in time</th>
                  <th valign="middle">Sign out time</th>
                  <th valign="middle">Status </th>
                  <th valign="middle">From Home</th>
                </tr>
            </thead>
            <tbody>
                @foreach($sign_ins as $sign_in)
                <tr class="odd gradeX" id="data-row-{{$sign_in->id}}">
                  <td valign="middle">
                  
                  {{ Carbon\Carbon::parse($sign_in->date)->format('l d, M Y') }}
                      
                  </td>
                  <td valign="middle">
                  
                  <?php if(isset($sign_in->sign_in_time)){?>
                  {{ Carbon\Carbon::parse($sign_in->sign_in_time)->format('l h:i:s A') }}
                  <?php ;}?>
                  </td>
                  <td valign="middle">
                  
                  <?php if(isset($sign_in->sign_out_time)){?>
                  {{ Carbon\Carbon::parse($sign_in->sign_out_time)->format('l h:i:s A') }}
                  <?php ;}?>
                  </td>
                  <td valign="middle">
                  {{$sign_in->status}}
                  </td>
                  <td valign="middle">
                   <?php if($sign_in->from_home == 1){?>
                   Yes
                   <?php ;}else{?>
                   No
                   <?php ;}?>
                    
                  </td>
                </tr>
                @endforeach

        </table>
        @endif
        
        @if($type == "penalties")
        <table class="table table-striped table-bordered table-hover table-dt" id="table-dt" >
            <thead>
                <tr class="tr-head">
                  <th valign="middle">Date</th>
                  <th valign="middle">Deducted days</th>
                  <th valign="middle">Deducted From</th>
                  <th valign="middle">Penalty type </th>
                </tr>
            </thead>
            <tbody>
                @foreach($penalties as $penalty)
                <tr class="odd gradeX" id="data-row-{{$penalty->id}}">
                  <td valign="middle">
                  
                  {{ Carbon\Carbon::parse($penalty->date)->format('l d, M Y') }}
                      
                    </td>
                  <td valign="middle">
                  {{$penalty->quantity}}
                  </td>
                  <td valign="middle">
                  {{$penalty->deducted_from}}
                  </td>
                  <td valign="middle" class="capitalize">
                   {{$penalty->penalty_reason}} 
                  </td>
                </tr>
                @endforeach

        </table>
        @endif
        
        
    </div>
</div>