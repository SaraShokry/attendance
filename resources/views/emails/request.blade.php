@extends('emails.master')
@section('content')
@if($req->status == 2)
<h3>New Request Submitted</h3>
@endif
@if($req->status == 1)
<h3>Your Request Has Been Accepted</h3>
@endif
@if($req->status == 0)
<h3>Your Request Has Been Rejected</h3>
@endif

<table width="50%" border="1" align="center" cellpadding="10" cellspacing="0" class="table_data">
  <tr>
    <td align="left" valign="middle" scope="col" style="color:#0088CC; font-weight:bold;">Employee</td>
    <td align="left" valign="middle" scope="col">{{$req->getEmployee->name}}</td>
  </tr>
  <tr>
    <td align="left" valign="middle" style="color:#0088CC; font-weight:bold;">Request type</td>
    <td align="left" valign="middle">
    <?php $req_type = str_replace("ves","ve",$req->type);
						$req_type= str_replace("ns","n",$req_type);
						echo str_replace("Home","Work From Home",$req_type);
					?>
    </td>
  </tr>
  <?php if($req->type == "Permissions"   || $req->type == "Half Day"  || $req->type == "Home" || $req->type == "Overtime"){?>
  <tr>
    <td align="left" valign="middle" style="color:#0088CC; font-weight:bold;">Day</td>
   
    <td align="left" valign="middle">{{$req->req_day}}</td>
    
  </tr>
  <?php ;}?>
  <?php if($req->type == "Permissions"   || $req->type == "Overtime" || $req->type == "Normal Leaves" || $req->type == "Emergency Leaves"){?>

  <tr>
    <td align="left" valign="middle" style="color:#0088CC; font-weight:bold;">From</td>
    <td align="left" valign="middle">
	<?php if($req->type == "Emergency Leaves" || $req->type == "Normal Leaves"){?>
                  <?php echo date("Y-m-d",strtotime($req->time_from));?>
                  <?php ;}else{?>
                  {{$req->time_from}}
                  <?php ;}?>
                  </td>
  </tr>
  <tr>
    <td align="left" valign="middle" style="color:#0088CC; font-weight:bold;">To</td>
    <td align="left" valign="middle">
    <?php if($req->type == "Emergency Leaves" || $req->type == "Normal Leaves"){?>
                  <?php echo date("Y-m-d",strtotime($req->time_to));?>
                  <?php ;}else{?>
                  {{$req->time_to}}
                  <?php ;}?>
    
    </td>
  </tr>
  <?php ;}?>
  <tr>
    <td align="left" valign="middle" style="color:#0088CC; font-weight:bold;">Interval</td>
    <td align="left" valign="middle">
    <?php if(isset($req->interval_in_days)){?>
                  {{$req->interval_in_days}} Day(s)
                  <?php ;}?>
                   <?php if(isset($req->interval_in_time)){?>
                  {{$req->interval_in_time}}
                  <?php ;}?>
    </td>
  </tr>
  <?php if($req->type == "Half Day" || $req->type == "Home" ){?>
  <tr>
    <td align="left" valign="middle" style="color:#0088CC; font-weight:bold;">
    @if($req->type == "Half Day")
                  	Half
                    @endif
                    @if($req->type == "Home")
                  	Exchanged day
                    @endif
                    
    
    </td>
    <td align="left" valign="middle">
    {{$req->add_info}}
    </td>
  </tr>
  <?php ;}?>
</table>


@stop