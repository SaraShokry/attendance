@extends('emails.master')
@section('content')
<h3>New Penalty</h3>

<table width="50%" border="1" align="center" cellpadding="10" cellspacing="0" class="table_data">
  <tr>
    <td align="left" valign="middle" scope="col" style="color:#0088CC; font-weight:bold;">Employee</td>
    <td align="left" valign="middle" scope="col">{{$penalty->getEmployee->name}}</td>
  </tr>
  <tr>
    <td align="left" valign="middle" style="color:#0088CC; font-weight:bold;">Date</td>
    <td align="left" valign="middle">
    {{ Carbon\Carbon::parse($penalty->date)->format('l d, M Y') }}
    </td>
  </tr>
  <tr>
    <td align="left" valign="middle" style="color:#0088CC; font-weight:bold;">Deducted Days</td>
   
    <td align="left" valign="middle">{{$penalty->quantity}}</td>
    
  </tr>

  <tr>
    <td align="left" valign="middle" style="color:#0088CC; font-weight:bold;">Deducted From</td>
    <td align="left" valign="middle">
	{{$penalty->deducted_from}}                  </td>
  </tr>
  <tr>
    <td align="left" valign="middle" style="color:#0088CC; font-weight:bold;">Reason</td>
    <td align="left" valign="middle" class="capitalize">
{{$penalty->penalty_reason}}     
    </td>
  </tr>
  
</table>


@stop