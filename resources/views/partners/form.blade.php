<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i> New  </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form action="{{route('admin.partners.save')}}" method="post" class="ajax_form">
        <div  id="main-form" novalidate method="POST" class="larajsval form"> 
            <div class="form-body">
            <div class="mt-element-ribbon bg-grey-steel">
                                                <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                                                    <div class="ribbon-sub ribbon-clip"></div> Main Info </div>
                                                <p class="ribbon-content">
                                                <div class="form-group">
                                                <div class="form-group">
                    <label>Company</label>
                    <select name="company_id" data-validation="required" data-name="Company" class="form-control">
                    	<option value="">Select Company</option>
                        @foreach($companies as $company)
                    	<option value="{{$company->id}}" <?php if($company->id == $partner->company_id){?>selected<?php ;}?>>{{$company->name}}</option>
                        @endforeach
                    </select>
                    <span class="error_message"></span>
                </div>
                                                  <div class="form-group">
                    <label>Name</label>
                    <input name="name" class="form-control" data-validation="required" data-name="Name" value="{{$partner->name}}"  >
                    <span class="error_message"></span>
                    <input type="hidden" name="id" id="id" value="{{$partner->id}}"  data-validation="" data-name="id"  />
                                                  </div>
                <div class="form-group">
                    <label>Email</label>
                    <input name="email" type="text" data-validation="required,email" data-name="Email"  class="form-control" value="{{$partner->email}}"  >
                    <span class="error_message"></span>
                </div>
                <div class="form-group">
                    <label>Phone</label>
                    <input name="phone" data-validation="required,number" data-name="Phone" class="form-control" value="{{$partner->phone}}"  >
                    <span class="error_message"></span>
                </div>
                <div class="form-group">
                    <label>password</label>
                    <input name="password" data-validation="required" data-name="password" type="password" class="form-control"  >
                    <span class="error_message"></span>
                </div>
                <div class="form-group">
                    <label>Retype Password</label>
                    <input name="password2" data-validation="required,match.password" data-name="password" type="password" class="form-control" >
                    <span class="error_message"></span>
                </div>
                
                
                
                
                
                
                
                
                </p>
                
                                            </div>
                                            
                                            </div>
            

            </div>
            <div class="form-actions">
                <div class="btn-set pull-right">
                    <button type="submit" class="btn btn-lg green"  >Save</button>
                </div>
            </div>
        </div>
        <input type="reset" class="hide resetForm" >
        </form>
        <!-- END FORM-->
    </div>
</div>
@include('quizzes.templates')