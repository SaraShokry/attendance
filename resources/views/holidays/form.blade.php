<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i> New  </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form action="{{route('admin.holidays.save')}}" method="post" class="ajax_form">
        <div  id="main-form" novalidate method="POST" class="larajsval form"> 
            <div class="form-body">
            <div class="mt-element-ribbon bg-grey-steel">
                                                <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                                                    <div class="ribbon-sub ribbon-clip"></div> Main Info </div>
                                                <p class="ribbon-content">
                                                <div class="form-group">
                                                <div class="form-group">
                                                @if(Auth::user()->role_id == 1)
                                                <div class="form-group">
                    <label>Office</label>
                    <select name="office_id" data-validation="required" data-name="Office" class="form-control">
                    	<option value="">Select Office</option>
                        @foreach($offices as $office)
                    	<option value="{{$office->id}}" <?php if($office->id == $holiday->office_id){?>selected<?php ;}?>>{{$office->name}}</option>
                        @endforeach
                    </select>
                    <span class="error_message"></span>
                </div>
                								@endif
                                                @if(Auth::user()->role_id == 2)
                    							<input type="hidden" name="office_id" id="office_id" value="{{Auth::user()->office_id}}"  data-validation="" data-name="office_id"  />
                								@endif
                                                  <div class="form-group">
                    <label>Name</label>
                    <input type="hidden" name="company_id"  data-validation="" data-name="company_id" value="{{Auth::user()->company_id}}"/>
                    <input name="name" class="form-control" data-validation="required" data-name="Name" value="{{$holiday->name}}" >
                    <span class="error_message"></span>
                    <input type="hidden" name="id" id="hiddenField" value="{{$holiday->id}}" data-validation="" data-name="id" />
                                                  </div>
                                                  <div class="form-group">
                    <label>Start Date</label>
                    <input name="start_date" class="form-control datePicker" data-validation="required" data-name="Start Date" <?php if(isset($holiday->start_date)){?>value="<?php echo date("d/m/Y",strtotime($holiday->start_date));?>"<?php ;}?> >
                    <span class="error_message"></span>
                                                  </div>
                                                  <div class="form-group">
                    <label>End Date</label>
                    <input name="end_date" class="form-control datePicker" data-validation="required" data-name="End Date" <?php if(isset($holiday->end_date)){?>value="<?php echo date("d/m/Y",strtotime($holiday->end_date));?>" <?php ;}?>>
                    <span class="error_message"></span>
                                                  </div>
                
                
                
                
                
                </p>
                
                                            </div>
                                            
                                            </div>
            

            </div>
            <div class="form-actions">
                <div class="btn-set pull-right">
                    <button type="submit" class="btn btn-lg green"  >Save</button>
                </div>
            </div>
        </div>
        <input type="reset" class="hide resetForm" >
        </form>
        <!-- END FORM-->
    </div>
</div>
