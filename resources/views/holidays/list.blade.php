<div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-globe"></i>Holidays
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
        </div>
    </div>
    <div class="portlet-body">
      <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                    <a href="{{route('admin.holidays.init')}}" class="pjax-link" >
                        <button class="btn green" id="add_new" data-action="{{route('admin.holidays.init')}}">

                            Add New <i class="fa fa-plus"></i>
                        </button>      
                        </a>                  
                    </div>
                </div>
                <div class="col-md-6"></div>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover table-dt" id="table-dt" >
            <thead>
                <tr class="tr-head">
                  <th valign="middle">
                      Name
                  </th>
                  <th valign="middle">Office</th>
                  <th valign="middle">Start</th>
                  <th valign="middle">End</th>
                    <th valign="middle">
                        Created at
                    </th>
                    <th valign="middle">
                        Action
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($holidays as $holiday)
                <tr class="odd gradeX" id="data-row-{{$holiday->id}}">
                  <td valign="middle">
                      {{$holiday->name}}
                    </td>
                  <td valign="middle">{{$holiday->getOffice->name}}</td>
                  <td valign="middle"><?php echo date("D d M Y",strtotime($holiday->start_date));?></td>
                  <td valign="middle"><?php echo date("D d M Y",strtotime($holiday->end_date));?></td>
                    <td valign="middle">
                        {{$holiday->created_at}}
                    </td>
                    <td valign="middle">
                        <a href="{{route('admin.holidays.edit',['id'=>$holiday->id])}}" class="btn green pjax-link" ><i class="fa fa-edit"></i> Edit</a> 
                        <a href="#" data-action="{{route('admin.holidays.delete',['id'=>$holiday->id])}}"  class="btn red delete_single" ><i class="fa fa-remove"></i> Delete</a> 
                    </td>
                </tr>
                @endforeach

        </table>
    </div>
</div>