<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i> New  </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form action="{{route('admin.companies.save')}}" method="post" class="ajax_form">
        <div  id="main-form" novalidate method="POST" class="larajsval form"> 
            <div class="form-body">
            <div class="mt-element-ribbon bg-grey-steel">
                                                <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                                                    <div class="ribbon-sub ribbon-clip"></div> Main Info </div>
                                                <p class="ribbon-content">
                                                <div class="form-group">
                                                  <div class="form-group">
                    <label>Name</label>
                    <input name="name" class="form-control" data-validation="required" data-name="Name" value="{{$company->name}}" >
                    <span class="error_message"></span>
                    <input type="hidden" name="id" id="hiddenField" value="{{$company->id}}" data-validation="" data-name="id" />
                                                  </div>
                
                
                
                
                
                </p>
                
                                            </div>
                                            
                                            </div>
            

            </div>
            <div class="form-actions">
                <div class="btn-set pull-right">
                    <button type="submit" class="btn btn-lg green"  >Save</button>
                </div>
            </div>
        </div>
        <input type="reset" class="hide resetForm" >
        </form>
        <!-- END FORM-->
    </div>
</div>
@include('quizzes.templates')