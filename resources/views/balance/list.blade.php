<div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-globe"></i>Balance
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
        </div>
    </div>
    <div class="portlet-body">
      <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                    <a href="{{route('admin.balance.init')}}" class="pjax-link" >
                        <button class="btn green" id="add_new" data-action="{{route('admin.balance.init')}}">

                            Add New <i class="fa fa-plus"></i>
                        </button>      
                        </a>                  
                    </div>
                </div>
                <div class="col-md-6"></div>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover table-dt" id="table-dt" >
            <thead>
                <tr class="tr-head">
                  <th valign="middle">
                      Employee
                  </th>
                  <th valign="middle">Type</th>
                  <th valign="middle">Quantity</th>
                    <th valign="middle">
                        Created at
                    </th>
                    <th valign="middle">
                        Action
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($balances as $balance)
                <tr class="odd gradeX" id="data-row-{{$balance->id}}">
                  <td valign="middle">
                      {{$balance->name}}
                    </td>
                  <td valign="middle">
                  {{$balance->type}}
                  </td>
                  <td valign="middle">
                  {{$balance->quantity}}
                  </td>
                    <td valign="middle">
                        {{$balance->created_at}}
                    </td>
                    <td valign="middle">
                        <a href="{{route('admin.balance.edit',['id'=>$balance->id])}}" class="btn green pjax-link" ><i class="fa fa-edit"></i> Edit</a> 
                        <a href="#" data-action="{{route('admin.balance.delete',['id'=>$balance->id])}}"  class="btn red delete_single" ><i class="fa fa-remove"></i> Delete</a> 
                    </td>
                </tr>
                @endforeach

        </table>
    </div>
</div>