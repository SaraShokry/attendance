<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i> New  </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form action="{{route('admin.balance.save')}}" method="post" class="ajax_form">
        <input type="hidden" name="id" id="id" value="{{$balance->id}}" />
<div  id="main-form" novalidate method="POST" class="larajsval form"> 
          <div class="form-body">
            <div class="mt-element-ribbon bg-grey-steel">
                                                <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                                                    <div class="ribbon-sub ribbon-clip"></div> Main Info </div>
                                                <p class="ribbon-content">
                                                <div class="form-group">
                                                <div class="form-group">
                    <label>Employee</label>
                    <select  name="emp_id" data-validation="required" data-name="Employee" class="form-control employees" >
                    	<option value="">Select Employee</option>
                        @foreach($employees as $employee)
                    	<option data-week_start = "{{$employee->week_start_day}}" value="{{$employee->id}}" <?php if($employee->id == $balance->emp_id){?>selected<?php ;}?>>{{$employee->name}}</option>
                        @endforeach
                    </select>
                    <span class="error_message"></span>
                </div>
                                                <div class="form-group"  >
                    <label>Type</label>
                    <select name="type" data-validation="required" data-name="This Field" class="form-control">
                    	<option value="">Select Type</option>
                        
                    	<option  value="Normal Leaves" <?php if($balance->type == "Normal Leaves"){?> selected="selected"<?php ;}?>>Normal Leaves</option>
                    	<option  value="Emergency Leaves" <?php if($balance->type == "Emergency Leaves"){?> selected="selected"<?php ;}?>>Emergency Leaves</option>
                    	<option  value="Permissions" <?php if($balance->type == "Permissions"){?> selected="selected"<?php ;}?>>Permissions</option>
                    	<option  value="Home" <?php if($balance->type == "Home"){?> selected="selected"<?php ;}?>>Work From Home</option>
                    </select>
                    <span class="error_message"></span>
                </div>
                                             
                                             
                                                  <div class="form-group dependent" data-depending-on="type" data-depending-value="Normal Leaves"  >
                    <label>Year</label>
                    <select name="normal_year" data-validation="required" data-name="This Field" class="form-control">
                    	<option value="">Select Year</option>
                        <?php for($y = 2007; $y<= 2100;$y++){?>
                    	<option  value="{{$y}}" <?php if($balance->year == $y){?> selected="selected"<?php ;}?>>{{$y}}</option>
                        <?php ;}?>
                    </select>
                    <span class="error_message"></span>
                </div>
                								
                                                <div class="dependent" data-depending-on="type" data-depending-value="Emergency Leaves">   
                                                  <div class="form-group"  >
                    <label>Year</label>
                    <select name="emergency_year" data-validation="required" data-name="This Field" class="form-control">
                    	<option value="">Select Year</option>
                        <?php for($y = 2007; $y<= 2100;$y++){?>
                    	<option  value="{{$y}}" <?php if($balance->year == $y){?> selected="selected"<?php ;}?>>{{$y}}</option>
                        <?php ;}?>
                    </select>
                    <span class="error_message"></span>
                </div>
                								</div>
                                                
                                                <div class="dependent" data-depending-on="type" data-depending-value="Permissions">   
                                                  <div class="form-group"  >
                    <label>Month</label>
                    <input class="monthPicker form-control" name="month" data-validation="required" data-name="This Field" value="{{$balance->month}}"  />
                    <span class="error_message"></span>
                </div>
                								</div>
                                                
                                                <div class="dependent" data-depending-on="type" data-depending-value="Home">   
                                                  <div class="form-group"  >
                    <label>Week</label>
                    <input data-weekpicker="weekpicker" data-week_start="7" class="form-control wpicker" name="week" data-validation="required" data-name="This Field" value="{{$balance->date_from}}-{{$balance->date_to}}"/>
                    <span class="error_message"></span>
                </div>
                </div>
                <div class="form-group"  >
                    <label>Quantity</label>
                    <input name="quantity"  class="form-control"  data-validation="required,number" data-name="This Field" value="<?php echo str_replace("-","",$balance->quantity);?>"/>
                    <span class="error_message"></span>
                </div>
                <div class="form-group"  >
                    <label>Type</label>
                    <select name="operation" data-validation="required" data-name="This Field" class="form-control">
                    	
                        
                    	<option  value="Add" <?php if (strpos($balance->quantity, '-') !== false) { ?><?php ;}else{?> selected="selected"<?php ;}?>>Add to balance</option>
                    	<option  value="Subtract" <?php if (strpos($balance->quantity, '-') !== false) { ?> selected="selected"<?php ;}?>>Remove from balance</option>
                    </select>
                    <span class="error_message"></span>
                </div>
                								
                
                
                
                
                </p>
                
                                            </div>
                                            
                                            </div>
            

            </div>
            <div class="form-actions">
                <div class="btn-set pull-right">
                    <button type="submit" class="btn btn-lg green"  >Save</button>
                </div>
            </div>
        </div>
        <input type="reset" class="hide resetForm" >
        </form>
        <!-- END FORM-->
    </div>
</div>
