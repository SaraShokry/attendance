<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i> New  </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <div  id="main-form" novalidate method="POST" class="larajsval form"> 
            <div class="form-body">
            <div class="mt-element-ribbon bg-grey-steel">
                                                <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                                                    <div class="ribbon-sub ribbon-clip"></div> Main Info </div>
                                                <p class="ribbon-content">
                                                <div class="form-group">
                                                  <div class="form-group">
                    <label>Name</label>
                    <input name="name" class="form-control" value="{{$candidate->name}}">
                    <input type="hidden" name="id" id="hiddenField" value="{{$candidate->id}}" />
                                                  </div>
                <div class="form-group">
                    <label>Email</label>
                    <input name="email" type="email" class="form-control" value="{{$candidate->email}}" >
                </div>
                <div class="form-group">
                    <label>Mobile</label>
                    <input name="mobile" class="form-control" value="{{$candidate->phone}}" >
                </div>
                <div class="form-group">
                    <label>Phase</label>
                    <select name="user_phase" class="form-control" >
                    	<option value="testing" <?php if($candidate->user_phase == "testing"){?> selected="selected"<?php ;}?>>Testing</option>
                    	<option value="testEvaluation" <?php if($candidate->user_phase == "testEvaluation"){?> selected="selected"<?php ;}?>>Test Evaluation</option>
                    	<option value="phoneInterview" <?php if($candidate->user_phase == "phoneInterview"){?> selected="selected"<?php ;}?>>Phone Interview</option>
                    	<option value="pilotTest" <?php if($candidate->user_phase == "pilotTest"){?> selected="selected"<?php ;}?>>Pilot Test</option>
                    	<option value="passedAll" <?php if($candidate->user_phase == "passedAll"){?> selected="selected"<?php ;}?>>Passed All</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Status</label>
                    <select name="user_status" class="form-control" >
                    	<option value="Pending" <?php if($candidate->user_status == "Pending"){?> selected="selected"<?php ;}?>>Pending</option>
                    	<option value="Approved" <?php if($candidate->user_status == "Approved"){?> selected="selected"<?php ;}?>>Approved</option>
                    	<option value="Rejected" <?php if($candidate->user_status == "Rejected"){?> selected="selected"<?php ;}?>>Rejected</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Quiz grade</label>
                    <input name="quiz" class="form-control" value="@if(isset($candidate->getGrades->quiz)){{$candidate->getGrades->quiz}}@endif" >
                </div>
                <div class="form-group">
                    <label>Fluency</label>
                    <input name="fluency" class="form-control" value="@if(isset($candidate->getGrades->quiz)){{$candidate->getGrades->fluency}}@endif" >
                </div>
                <div class="form-group">
                    <label>Vocab</label>
                    <input name="vocab" class="form-control" value="@if(isset($candidate->getGrades->quiz)){{$candidate->getGrades->vocab}}@endif" >
                </div>
                <div class="form-group">
                    <label>Grammar</label>
                    <input name="grammar" class="form-control" value="@if(isset($candidate->getGrades->quiz)){{$candidate->getGrades->grammar}}@endif" >
                </div>
                <div class="form-group">
                    <label>Accent</label>
                    <input name="accent" class="form-control" value="@if(isset($candidate->getGrades->quiz)){{$candidate->getGrades->accent}}@endif" >
                </div>
                <div class="form-group">
                    <label>Pilot test grade</label>
                    <input name="pilot" class="form-control" value="@if(isset($candidate->getGrades->quiz)){{$candidate->getGrades->pilot}}@endif" >
                </div>
               
                
                
                
                
                </p>
                
                                            </div>
                                            
                                            </div>
            

            </div>
            <div class="form-actions">
                <div class="btn-set pull-right">
                    <button type="button" class="btn btn-lg green" id="save-all-edit" data-action="{{route('admin.candidates.saveEdit')}}">Save</button>
                </div>
            </div>
        </div>
        <!-- END FORM-->
    </div>
</div>
