<form class="ajax_form" method="post" action="/admin/tasks/store">
{{ csrf_field() }}
  <input type="hidden" name="id" id="id" value="<?php if(isset($task)){?>{{$task->id}}<?php ;}?>" />
<div  id="main-form" novalidate  class="form-horizontal form"> 
    <div class="global-errors alert alert-danger hidden"></div>
            <div class="form-body">
<div class="mt-element-ribbon bg-grey-steel">
                    <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                        <div class="ribbon-sub ribbon-clip"></div> Task Details </div>
                    <div class="ribbon-content">
                        
                        <div class="form-group">
                            <label>Name</label>
                            <input name="name" class="form-control" value="<?php if(isset($task)){?>{{$task->name}}<?php ;}?>">
                        </div>
                        <div class="form-group">
                            <label>Software</label>
                            <select name="software" class="form-control">
                            	<option value="">Select software</option>
                                <option value="InDesign" <?php if(isset($task)){?><?php if($task->software == "InDesign"){?> selected="selected" <?php ;}?><?php ;}?>>InDesign</option>
                                <option value="Frame Maker" <?php if(isset($task)){?><?php if($task->software == "Frame Maker"){?> selected="selected" <?php ;}?><?php ;}?>>Frame Maker</option>
                                <option value="Photoshop" <?php if(isset($task)){?><?php if($task->software == "Photoshop"){?> selected="selected" <?php ;}?><?php ;}?>>Photoshop</option>
                                <option value="Illustrator" <?php if(isset($task)){?><?php if($task->software == "Illustrator"){?> selected="selected" <?php ;}?><?php ;}?>>Illustrator</option>
                                <option value="Word" <?php if(isset($task)){?><?php if($task->software == "Word"){?> selected="selected" <?php ;}?><?php ;}?>>Word</option>
                                <option value="PowerPoint" <?php if(isset($task)){?><?php if($task->software == "PowerPoint"){?> selected="selected" <?php ;}?><?php ;}?>>PowerPoint</option>
                                <option value="Excel" <?php if(isset($task)){?><?php if($task->software == "Excel"){?> selected="selected" <?php ;}?><?php ;}?>>Excel</option>
                                <option value="Quark" <?php if(isset($task)){?><?php if($task->software == "Quark"){?> selected="selected" <?php ;}?><?php ;}?>>Quark</option>
                                <option value="Quick Silver" <?php if(isset($task)){?><?php if($task->software == "Quick Silver"){?> selected="selected" <?php ;}?><?php ;}?>>Quick Silver</option>
                                <option value="Corel Ventura" <?php if(isset($task)){?><?php if($task->software == "Corel Ventura"){?> selected="selected" <?php ;}?><?php ;}?>>Corel Ventura</option>
                                <option value="Corel Draw" <?php if(isset($task)){?><?php if($task->software == "Corel Draw"){?> selected="selected" <?php ;}?><?php ;}?>>Corel Draw</option>
                                <option value="Visio" <?php if(isset($task)){?><?php if($task->software == "Visio"){?> selected="selected" <?php ;}?><?php ;}?>>Visio</option>
                                <option value="Publisher" <?php if(isset($task)){?><?php if($task->software == "Publisher"){?> selected="selected" <?php ;}?><?php ;}?>>Publisher</option>
                                <option value="Autocad" <?php if(isset($task)){?><?php if($task->software == "Autocad"){?> selected="selected" <?php ;}?><?php ;}?>>Autocad</option>
                                <option value="Page Maker" <?php if(isset($task)){?><?php if($task->software == "Page Maker"){?> selected="selected" <?php ;}?><?php ;}?>>Page Maker</option>
                                <option value="Articulate" <?php if(isset($task)){?><?php if($task->software == "Articulate"){?> selected="selected" <?php ;}?><?php ;}?>>Articulate</option>
                                <option value="Captivate" <?php if(isset($task)){?><?php if($task->software == "Captivate"){?> selected="selected" <?php ;}?><?php ;}?>>Captivate</option>
                                <option value="Lectora" <?php if(isset($task)){?><?php if($task->software == "Lectora"){?> selected="selected" <?php ;}?><?php ;}?>>Lectora</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Estimated time</label>
                            <div class="row">
                                <div class="col-sm-4">
                                	<input name="estimated_time_hours" class="form-control" value="<?php if(isset($task)){?>{{$task->estimated_time_hours}}<?php ;}?>">    
                                </div>
                                <div class="col-sm-2">
                                    Hours
                                </div>
                                <div class="col-sm-4">
                                	<input name="estimated_time_mins" class="form-control" value="<?php if(isset($task)){?>{{$task->estimated_time_mins}}<?php ;}?>">        
                                </div>
                                <div class="col-sm-2">
                                    Minutes
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Project Type</label>
                            <select name="project_type" class="form-control">
                            	<option value="">Select Type</option>
                                <option value="Update" <?php if(isset($task)){?><?php if($task->project_type == "Update"){?> selected="selected" <?php ;}?><?php ;}?>>Update</option>
                                <option value="Resolving" <?php if(isset($task)){?><?php if($task->project_type == "Resolving"){?> selected="selected" <?php ;}?><?php ;}?>>Resolving</option>
                                <option value="New" <?php if(isset($task)){?><?php if($task->project_type == "New"){?> selected="selected" <?php ;}?><?php ;}?>>New</option>
                                <option value="Client correction" <?php if(isset($task)){?><?php if($task->project_type == "Client correction"){?> selected="selected" <?php ;}?><?php ;}?>>Client correction</option>
                                <option value="QC" <?php if(isset($task)){?><?php if($task->project_type == "QC"){?> selected="selected" <?php ;}?><?php ;}?>>QC</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Language pair</label>
                            <div class="row">
                                <div class="col-sm-6">
                                    <select name="source" class="form-control">
                                        <option value="">Select source</option>
                                        @foreach($languages as $language)
                                        <option value="{{$language->language}}" <?php if(isset($task)){?><?php if($task->language_source == $language->language){?> selected="selected" <?php ;}?><?php ;}?>>{{$language->language}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-6">
                                    <select name="target" class="form-control" >
                                        <option value="">Select target</option>
                                        @foreach($languages as $language)
                                        <option value="{{$language->language}}" <?php if(isset($task)){?><?php if($task->language_target == $language->language){?> selected="selected" <?php ;}?><?php ;}?>>{{$language->language}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Start time</label>
                            <input name="start_time" class="form-control timepicker" value="<?php if(isset($task)){?>{{$task->start_time}}<?php ;}?>" <?php if(isset($task)){?> style="display:none;"<?php ;}?>>
                            <?php if(isset($task)){?></br>{{ Carbon\Carbon::parse($task->start_time)->format('D d, M Y H:i') }}<?php ;}?>
                        </div>
                        <div class="form-group">
                            <label>Email subject</label>
                            <input name="email_subject" class="form-control" value="<?php if(isset($task)){?>{{$task->email_subject}}<?php ;}?>">
                        </div>
                        <div class="form-group">
                            <label>Share path</label>
                            <input name="share_path" class="form-control" value="<?php if(isset($task)){?>{{$task->share_path}}<?php ;}?>">
                        </div>
                        <input type="submit" name="button" id="button" value="Submit" class="btn blue" />
                        
                        
                    </div>
              </div>
    </div>
                </div>

</form>