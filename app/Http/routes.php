<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'userController@index')->name('index');
Route::get('/cronjob', 'cronjobController@run')->name('cronjob');
//Route::get('/fixingBug', 'cronjobController@fixingBug')->name('fixingBug');




Route::group(['middleware' => ['auth'], 'prefix' => 'admin/users', 'as' => 'admin.users.'], function () {
    Route::get('/{status}', 'UserController@index')->name('index');
    Route::post('init', 'UserController@init')->name('init');
    Route::get('{id}/edit', 'UserController@edit')->name('edit');
    Route::delete('{id}/delete', 'UserController@delete')->name('delete');
    Route::post('{id}/recommission', 'UserController@recommission')->name('recommission');
    Route::get('create', 'UserController@create')->name('create');
    Route::delete('delete_multiple', 'UserController@deleteMultiple')->name('delete_multiple');
    Route::post('store', 'UserController@store')->name('store');
    Route::patch('{id}/update', 'UserController@update')->name('update');
});



Route::group(['middleware' => ['auth'], 'prefix' => "admin/companies", 'as' => "admin.companies."], function () {
    Route::get('/', 'companyController@index')->name('index');
    Route::delete('{id}/delete', 'companyController@delete')->name('delete');
    Route::get('{id}/edit', 'companyController@edit')->name('edit');
    Route::get('init', 'companyController@init')->name('init');
    Route::post('save', 'companyController@save')->name('save');

});

Route::group(['middleware' => ['auth'], 'prefix' => "admin/partners", 'as' => "admin.partners."], function () {
    Route::get('/', 'partnerController@index')->name('index');
    Route::delete('{id}/delete', 'partnerController@delete')->name('delete');
    Route::get('{id}/edit', 'partnerController@edit')->name('edit');
    Route::get('init', 'partnerController@init')->name('init');
    Route::post('save', 'partnerController@save')->name('save');

});
Route::group(['middleware' => ['auth'], 'prefix' => "admin/offices", 'as' => "admin.offices."], function () {
    Route::get('/', 'officeController@index')->name('index');
    Route::delete('{id}/delete', 'officeController@delete')->name('delete');
    Route::get('{id}/edit', 'officeController@edit')->name('edit');
    Route::get('init', 'officeController@init')->name('init');
    Route::post('save', 'officeController@save')->name('save');

});

Route::group(['middleware' => ['auth'], 'prefix' => "admin/employees", 'as' => "admin.employees."], function () {
    Route::get('/', 'employeeController@index')->name('index');
    Route::delete('{id}/delete', 'employeeController@delete')->name('delete');
    Route::get('{id}/edit', 'employeeController@edit')->name('edit');
    Route::get('init', 'employeeController@init')->name('init');
    Route::post('save', 'employeeController@save')->name('save');

});

Route::group(['middleware' => ['auth'], 'prefix' => "admin/sign_ins", 'as' => "admin.sign_ins."], function () {
    Route::get('/', 'sign_in_editingController@index')->name('index');
    Route::get('/no_sign_outs', 'sign_in_editingController@noSignOuts')->name('no_sign_outs');
    Route::delete('{id}/delete', 'sign_in_editingController@delete')->name('delete');
    Route::get('{id}/edit', 'sign_in_editingController@edit')->name('edit');
    Route::get('init', 'sign_in_editingController@init')->name('init');
    Route::post('save', 'sign_in_editingController@save')->name('save');

});
Route::group(['middleware' => ['auth'], 'prefix' => "admin/departments", 'as' => "admin.departments."], function () {
    Route::get('/', 'departmentController@index')->name('index');
    Route::delete('{id}/delete', 'departmentController@delete')->name('delete');
    Route::get('{id}/edit', 'departmentController@edit')->name('edit');
    Route::get('init', 'departmentController@init')->name('init');
    Route::post('save', 'departmentController@save')->name('save');

});
Route::group(['middleware' => ['auth'], 'prefix' => "admin/positions", 'as' => "admin.positions."], function () {
    Route::get('/', 'positionController@index')->name('index');
    Route::delete('{id}/delete', 'positionController@delete')->name('delete');
    Route::get('{id}/edit', 'positionController@edit')->name('edit');
    Route::get('init', 'positionController@init')->name('init');
    Route::post('save', 'positionController@save')->name('save');

});

Route::group(['middleware' => ['auth'], 'prefix' => "admin/holidays", 'as' => "admin.holidays."], function () {
    Route::get('/', 'holidayController@index')->name('index');
    Route::delete('{id}/delete', 'holidayController@delete')->name('delete');
    Route::get('{id}/edit', 'holidayController@edit')->name('edit');
    Route::get('init', 'holidayController@init')->name('init');
    Route::post('save', 'holidayController@save')->name('save');

});
Route::group(['middleware' => ['auth'], 'prefix' => "admin/holiday_profiles", 'as' => "admin.holiday_profiles."], function () {
    Route::get('/', 'holiday_profileController@index')->name('index');
    Route::delete('{id}/delete', 'holiday_profileController@delete')->name('delete');
    Route::get('{id}/edit', 'holiday_profileController@edit')->name('edit');
    Route::get('init', 'holiday_profileController@init')->name('init');
    Route::post('save', 'holiday_profileController@save')->name('save');

});
Route::group(['middleware' => ['auth'], 'prefix' => "admin/att_profiles", 'as' => "admin.att_profiles."], function () {
    Route::get('/', 'att_profileController@index')->name('index');
    Route::delete('{id}/delete', 'att_profileController@delete')->name('delete');
    Route::get('{id}/edit', 'att_profileController@edit')->name('edit');
    Route::get('init', 'att_profileController@init')->name('init');
    Route::post('save', 'att_profileController@save')->name('save');

});
Route::group(['middleware' => ['auth'], 'prefix' => "admin/requests", 'as' => "admin.requests."], function () {
    Route::get('/{type}', 'requestController@index')->name('index');
    Route::delete('{id}/delete', 'requestController@delete')->name('delete');
    Route::get('{id}/edit', 'requestController@edit')->name('edit');
    Route::get('init', 'requestController@init')->name('init');
    Route::post('saveEmergency', 'requestController@saveEmergency')->name('saveEmergency');
    Route::post('savePermission', 'requestController@savePermission')->name('savePermission');
    Route::post('saveNormal', 'requestController@saveNormal')->name('saveNormal');
    Route::post('saveFlexHome', 'requestController@saveFlexHome')->name('saveFlexHome');
    Route::post('saveExchangeHome', 'requestController@saveExchangeHome')->name('saveExchangeHome');
    Route::post('saveHalfDay', 'requestController@saveHalfDay')->name('saveHalfDay');
    
});

Route::group(['middleware' => ['auth'], 'prefix' => "admin/reqs", 'as' => "admin.reqs."], function () {
    Route::get('/{type}', 'inbound_requestController@index')->name('index');
    Route::get('/user_requests/{user_id}/{status}/{taken}', 'inbound_requestController@userRequests')->name('userRequests');
    Route::delete('{id}/delete', 'inbound_requestController@delete')->name('delete');
    Route::delete('{id}/accept', 'inbound_requestController@accept')->name('accept');
    Route::delete('{id}/reject', 'inbound_requestController@reject')->name('reject');
    Route::delete('{id}/cancel', 'inbound_requestController@cancel')->name('cancel');
    Route::get('init', 'inbound_requestController@init')->name('init');
    Route::post('save', 'inbound_requestController@save')->name('save');
    Route::post('edit_signout_interval', 'inbound_requestController@editSignoutInterval')->name('edit_signout_interval');
    

});

Route::group(['middleware' => ['auth'], 'prefix' => "admin/user_reports", 'as' => "admin.user_reports."], function () {
    Route::get('/{type}', 'user_reportController@index')->name('index');
});

Route::group(['middleware' => ['auth'], 'prefix' => "admin/sign_in_out", 'as' => "admin.sign_in_out."], function () {
    Route::get('/sign_in', 'sign_inController@sign_in')->name('sign_in');
    Route::get('/sign_out', 'sign_inController@sign_out')->name('sign_out');
    Route::get('/dismissWorkFromHome', 'sign_inController@dismissWorkFromHome')->name('dismissWorkFromHome');
    Route::get('/revertSignOut', 'sign_inController@revertSignOut')->name('revertSignOut');
    Route::get('/refreshSignInOut', 'sign_inController@refreshSignInOut')->name('refreshSignInOut');
});

Route::group(['middleware' => ['auth'], 'prefix' => "admin/balance", 'as' => "admin.balance."], function () {
    Route::get('/', 'balanceController@index')->name('index');
    Route::delete('{id}/delete', 'balanceController@delete')->name('delete');
    Route::get('{id}/edit', 'balanceController@edit')->name('edit');
    Route::get('init', 'balanceController@init')->name('init');
    Route::post('save', 'balanceController@save')->name('save');

});
Route::auth();

