<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Department;
use App\Office;
use Auth;
use App\User;

class departmentController extends Controller
{
	public function __construct(){
		$this->logged_user = Auth::user();
        
	}
    public function index() {
        if($this->logged_user->office_id !=0){
            $departments = Department::where('admin_show',1)->where('company_id',$this->logged_user->company_id)->where('office_id',$this->logged_user->office_id)->get();
        }else{
            $departments = Department::where('admin_show',1)->where('company_id',$this->logged_user->company_id)->get();
        }
    	
        $data = [];
        $data['partialView'] = 'departments.list';
        $data['departments'] = $departments;
        
        return view('departments.base', $data);
    }

    

   

    public function init(){
    	$department = new Department();
    	$department->save();
    	return redirect('/admin/departments/'.$department->id.'/edit');
        
    }

    public function edit($id){
    	$department =  Department::find($id);
    	$department->save();
        $data = [];
       
        $offices = Office::where('admin_show',1)->where('company_id',$this->logged_user->company_id)->orderBy('name')->get();
        $data['offices'] = $offices;
        $data['department'] = $department;
        $data['partialView'] = 'departments.form';
        return view('departments.base', $data);
    }
    

    public function save(Request $request){
        $data = $request->input();
        $department = Department::find($data['id']);
        $department->name = $data['name'];
        $department->company_id = $data['company_id'];
        $department->office_id = $data['office_id'];
        $department->admin_show = 1;
        if($department->save()){
            $data = [];
            $data['status'] = 'success';
            $data['page'] = '/admin/departments';
            $data['msg'] = "Saved Successfully";
            return response()->json(
                        $data
            );  
        }else{
        	$data = [];
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "There was an error";
            return response()->json(
                        $data
            );  

        }
  
    }
  
    public function delete($id){
        $count = User::where('department_id',$id)->count();
        if($count ==0){
            $delete = Department::find($id)->delete();
        }else{
            abort(404);
        }
    }
}
