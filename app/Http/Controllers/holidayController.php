<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use App\Holiday;
use App\Office;
use App\Holidays_profile;
class holidayController extends Controller
{
    public function __construct(){
		$this->logged_user = Auth::user();

	}
    public function index() {
        if($this->logged_user->office_id !=0){
            $holidays = Holiday::where('admin_show',1)->where('company_id',$this->logged_user->company_id)->where('office_id',$this->logged_user->office_id)->get();
        }else{
            $holidays = Holiday::where('admin_show',1)->where('company_id',$this->logged_user->company_id)->get();
        }
    	
        $data = [];
        $data['partialView'] = 'holidays.list';
        $data['holidays'] = $holidays;
        
        return view('holidays.base', $data);
    }

    public function init(){
    	$holiday = new Holiday();
    	$holiday->save();
    	return redirect('/admin/holidays/'.$holiday->id.'/edit');
        
    }

    public function edit($id){
    	$holiday =  Holiday::find($id);
    	$holiday->save();
        $data = [];
       
        $offices = Office::where('admin_show',1)->where('company_id',$this->logged_user->company_id)->orderBy('name')->get();
        $data['offices'] = $offices;
        $data['holiday'] = $holiday;
        $data['partialView'] = 'holidays.form';
        return view('holidays.base', $data);
    }
    

    public function save(Request $request){
        $data = $request->input();
        // Date Validation
        if(strtotime($data['start_date'])>strtotime($data['end_date'])){
        	$data = [];
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "The start date is after the end date";
            return response()->json(
                        $data
            ); 
        }
        $holiday = Holiday::find($data['id']);
        $holiday->name = $data['name'];
        $holiday->company_id = $data['company_id'];
        $holiday->office_id = $data['office_id'];
        $holiday->start_date = date("Y-m-d",strtotime($data['start_date']));
        $holiday->end_date = date("Y-m-d",strtotime($data['end_date']));
        $holiday->admin_show = 1;
        if($holiday->save()){
            $data = [];
            $data['status'] = 'success';
            $data['page'] = '/admin/holidays';
            $data['msg'] = "Saved Successfully";
            return response()->json(
                        $data
            );  
        }else{
        	$data = [];
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "There was an error";
            return response()->json(
                        $data
            );  

        }
        
    }
    
    public function delete($id){
        $count = Holidays_profile::where('holiday_id',$id)->count();
        if($count ==0){
            $delete = Holiday::find($id)->delete();
        }else{
            abort(404);
        }
       
    }
}
