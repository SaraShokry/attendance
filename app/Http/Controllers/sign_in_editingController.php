<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Helpers\Helpers;
use App\Http\Requests;
use App\Http\Helpers\SignInOut;
use Auth ;
use App\User;
use App\Office;
use DB;
use App\Sign_in_out;
use App\Managers_employee;
use App\Balance;


class sign_in_editingController extends Controller
{
    public function __construct(){
		$this->logged_user = Auth::user();
	}
    public function index() {
        if($this->logged_user->office_id !=0){
            $sign_ins = DB::table('sign_in')
	        ->join('users', 'users.id', '=', 'sign_in.emp_id')
	        ->where('users.office_id',$this->logged_user->office_id)
	        ->select('users.name','sign_in.*')
	        ->get();
        }else{
            $sign_ins = DB::table('sign_in')
	        ->join('users', 'users.id', '=', 'sign_in.emp_id')
	        ->where('users.company_id',$this->logged_user->company_id)
	        ->select('users.name','sign_in.*')
	        ->get();
        }
        $data = [];
        $data['partialView'] = 'sign_ins.list';
        $data['sign_ins'] = $sign_ins; 
        $data['type'] = "sign_ins";
        return view('sign_ins.base', $data);
    }
    public function noSignOuts() {
        $user_id = $this->logged_user->id;
        // get employees that the logged user is a copy manager for them
        $get_employees_of_manager = Managers_employee::where('manager_id',$this->logged_user->id)->get();
       
        $get_employees_of_manager_array = array();
        foreach($get_employees_of_manager as $employee){
            array_push($get_employees_of_manager_array, $employee->employee_id);
        }
        // Hr 
        if($this->logged_user->role_id == 2  ){
        $sign_ins = DB::table('sign_in')
            ->join('users', 'users.id', '=', 'sign_in.emp_id')
            ->where('users.office_id',$this->logged_user->office_id)
            ->whereNull('sign_in.sign_out_time')
            ->whereNotNull('sign_in.sign_in_time')
            ->select('users.name','sign_in.*')
            ->get();
        }
        // Business Partner
        elseif ($this->logged_user->role_id == 1) {
            $sign_ins = DB::table('sign_in')
            ->join('users', 'users.id', '=', 'sign_in.emp_id')
            ->where('users.company_id',$this->logged_user->company_id)
            ->whereNull('sign_in.sign_out_time')
            ->whereNotNull('sign_in.sign_in_time')
            ->select('users.name','sign_in.*')
            ->get();
        }
        // Manager
        else{

$sign_ins = DB::table('sign_in')
            ->join('users', 'users.id', '=', 'sign_in.emp_id')
            ->where(function ($query) use ($get_employees_of_manager_array,$user_id) {
                $query->whereIn('sign_in.emp_id',$get_employees_of_manager_array)
                      ->orWhere('users.manager_id',$user_id);
            })->where(function ($query) use ($get_employees_of_manager_array,$user_id) {
                $query->whereNull('sign_in.sign_out_time')
                    ->whereNotNull('sign_in.sign_in_time');
            })->select('users.name','sign_in.*')
            ->get();


        }
        $data = [];
        $data['partialView'] = 'sign_ins.list';
        $data['sign_ins'] = $sign_ins; 
        $data['type'] = "no_sign_outs";
        return view('sign_ins.base', $data);

    }



    public function init(){
    	$sign_in = new Sign_in_out();
    	$sign_in->save();
    	return redirect('/admin/sign_ins/'.$sign_in->id.'/edit');
    }

    public function edit($id){
    	$sign_in =  Sign_in_out::find($id);
    	
    	$offices = Office::where('admin_show',1)->where('company_id',$this->logged_user->company_id)->orderBy('name')->get();
    	
    	if($this->logged_user->office_id !=0){
    		$employees = User::where('admin_show',1)->where('company_id',$this->logged_user->company_id)->where('office_id',$this->logged_user->office_id)->orderBy('name')->get();
    	}else{
    		$employees = User::where('admin_show',1)->where('company_id',$this->logged_user->company_id)->orderBy('name')->get();
    	}
    	
        $data = [];
        $data['offices'] = $offices;
        $data['employees'] = $employees;
        $data['sign_in'] = $sign_in;
        $data['partialView'] = 'sign_ins.form';
        $data['type'] = "sign_ins";
        return view('sign_ins.base', $data);
    }

    public function save(Request $request){
        $data = $request->input();
        if($data['day_type'] == ""){
            $data['day_type'] = "normal";
        }
        $sign_in = Sign_in_out::find($data['id']);

        $data['sign_in_time'] = date("Y-m-d H:i:s",strtotime($data['sign_in_time']));
        
        
        $sign_in_time_data = Helpers::getFullDayData($data['sign_in_time'],$data['emp_id'],$data['day_type']);
        $sign_in_date = $sign_in_time_data['date'];
        if($sign_in->date != $sign_in_date && $data['user_status'] == "attended"){
            $data = [];
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "The sign in time is in another workday";
            return response()->json(
                        $data
            ); 

        }
        if($data['sign_out_time'] != ""){
            $data['sign_out_time'] = date("Y-m-d H:i:s",strtotime($data['sign_out_time']));
            $sign_out_time_data = Helpers::getFullDayData($data['sign_out_time'],$data['emp_id'],$data['day_type']);
            $sign_out_date = $sign_out_time_data['date'];
            if($sign_in->date != $sign_out_date ){
                $data = [];
                $data['status'] = 'error';
                $data['page'] = 'none';
                $data['msg'] = "The sign out time is in another workday";
                return response()->json(
                            $data
                ); 

            }
            if(strtotime($data['sign_out_time']) < strtotime($data['sign_in_time'])  ){
                $data = [];
                $data['status'] = 'error';
                $data['page'] = 'none';
                $data['msg'] = "The sign out time is before the sign in time";
                return response()->json(
                            $data
                ); 

            }
        }

        if($data['user_status'] == "attended"){
            $overRide = SignInOut::overRideSignInOut($sign_in->id,$data['sign_in_time'],$data['sign_out_time'],$data['home'],$data['day_type']);
        }
        elseif($data['user_status'] == "Leave"){
            $remaining_leaves = Helpers::getRemainingLeaves($sign_in->emp_id,date("Y",strtotime($sign_in->date)),"Normal",$profile_type="normal");
            if($remaining_leaves < 1){
                $data = [];
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "This user does not have enough normal leaves";
            return response()->json(
                        $data
            ); 

            }
            $rollback = Helpers::rollbackSignInOut($sign_in->id);
            $new_sign_in = new Sign_in_out();
            $new_sign_in->date = $sign_in->date;

            $new_sign_in->emp_id = $sign_in->emp_id;
            $new_sign_in->status = "Leave";
            $new_sign_in->day_type = "normal";
            $new_sign_in->save();
            $balance = new Balance();
            $balance->year = date("Y",strtotime($sign_in->date));
            $balance->quantity = -1;
            $balance->type = "Normal Leaves";
            $balance->emp_id = $sign_in->emp_id;
            $balance->admin_show = 1;
            $balance->save();
            


        }
        elseif($data['user_status'] == "Weekend"){
            
            $rollback = Helpers::rollbackSignInOut($sign_in->id);
            $new_sign_in = new Sign_in_out();
            $new_sign_in->date = $sign_in->date;

            $new_sign_in->emp_id = $sign_in->emp_id;
            $new_sign_in->status = "Weekend";
            $new_sign_in->day_type = "normal";
            $new_sign_in->save();
            


        }
        
        
            $data = [];
            $data['status'] = 'success';
            $data['page'] = '/admin/sign_ins';
            $data['msg'] = "Saved Successfully";
            return response()->json(
                        $data
            );  
        
        	$data = [];
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "There was an error";
            return response()->json(
                        $data
            );  
        
    }
    
    public function delete($id){
       $delete = Sign_in_out::find($id)->delete();
    }
}
