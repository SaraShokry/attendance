<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use App\Position;
use App\User;
use App\Department;
use App\Employee_role;
use App\Att_profile;
use App\Office;
use App\Company;
use Hash;
use App\Managers_employee;

class employeeController extends Controller
{
	public function __construct(){
		$this->logged_user = Auth::user();

	}
    public function index() {
        if($this->logged_user->office_id !=0){
            $employees = User::where('admin_show',1)->where('company_id',$this->logged_user->company_id)->where('office_id',$this->logged_user->office_id)->get();
        }else{
            $employees = User::where('admin_show',1)->where('company_id',$this->logged_user->company_id)->get();
        }
        $data = [];
        $data['partialView'] = 'employees.list';
        $data['employees'] = $employees; 
        return view('employees.base', $data);
    }

    public function init(){
    	$employee = new User();
    	$employee->save();
    	return redirect('/admin/employees/'.$employee->id.'/edit');
    }

    public function edit($id){
    	$employee =  User::find($id);
    	$employee->save();
    	$offices = Office::where('admin_show',1)->where('company_id',$this->logged_user->company_id)->orderBy('name')->get();
    	$entriess = Managers_employee::where('employee_id',$employee->id)->get();
        $entries = [];
        foreach($entriess as $entry){
        	array_push($entries, $entry->manager_id);
        }
    	if($this->logged_user->office_id !=0){
    		$positions = Position::where('admin_show',1)->where('company_id',$this->logged_user->company_id)->where('office_id',$this->logged_user->office_id)->orderBy('name')->get();
    		$departments = Department::where('admin_show',1)->where('company_id',$this->logged_user->company_id)->where('office_id',$this->logged_user->office_id)->orderBy('name')->get();
    		$profiles = Att_profile::where('admin_show',1)->where('company_id',$this->logged_user->company_id)->where('office_id',$this->logged_user->office_id)->orderBy('name')->get();
    		$managers = User::where('admin_show',1)->where('id','!=',$employee->id)->whereIn('role_id',array(1, 2, 3))->where('company_id',$this->logged_user->company_id)->where('office_id',$this->logged_user->office_id)->orWhere('role_id',1)->orderBy('name')->get();
    	}else{
    		$positions = Position::where('admin_show',1)->where('company_id',$this->logged_user->company_id)->orderBy('name')->get();
    		$departments = Department::where('admin_show',1)->where('company_id',$this->logged_user->company_id)->orderBy('name')->get();
    		$profiles = Att_profile::where('admin_show',1)->where('company_id',$this->logged_user->company_id)->orderBy('name')->get();
    		$managers = User::where('admin_show',1)->where('id','!=',$employee->id)->whereIn('role_id',array(1, 2, 3))->where('company_id',$this->logged_user->company_id)->orderBy('name')->get();
    	}
    	$roles = Employee_role::where('admin_show',1)->where('id','>',$this->logged_user->role_id)->get();
        $data = [];
        $data['entries'] = $entries;
        $data['offices'] = $offices;
        $data['positions'] = $positions;
        $data['departments'] = $departments;
        $data['profiles'] = $profiles;
        $data['managers'] = $managers;
        $data['employee'] = $employee;
        $data['roles'] = $roles;
        $data['partialView'] = 'employees.form';
        return view('employees.base', $data);
    }

    public function save(Request $request){
        $data = $request->input();
        //Validations
        $check_email = User::where('email',$data['email'])->where('id','!=',$data['id'])->count();
        if($check_email !=0){
            $data = [];
            $data['status'] = 'error';
            
            $data['msg'] = "Email already exists";
            return response()->json(
                        $data
            );
        }
        // Work from home number of days validation
        if(isset($data['home_days'])){
            $number_of_home_days = count($data['home_days']);
            $max_per_week = $data['max_homeDays_per_week'];

            if($number_of_home_days>$max_per_week){
               $data = [];
                $data['status'] = 'error';
                
                $data['msg'] = "Number of default work days chosen can not exceed maximum number of work from home days per week";
                return response()->json(
                            $data
                ); 
            }
        }
        // Strats at validation
        $starts_at = date("Y-m-d",strtotime($data['starts_at']));
        $office = Office::find($data['office_id']);
        $employee = User::find($data['id']);
        date_default_timezone_set($office->time_zone);
        $now = date("Y-m-d");
        if($employee->user_created_at){
            $created_at = $employee->user_created_at;
        }else{
            $created_at = $now;
        }
        
        if(strtotime($starts_at) < strtotime($created_at)){
            $data = [];
            $data['status'] = 'error';
            
            $data['msg'] = "The date chosen as the start date for considering this employee absent is invalid";
            return response()->json(
                        $data
            );

        }

        $employee = User::find($data['id']);
        $employee->office_id = $data['office_id'];
        $employee->company_id = $data['company_id'];
        $employee->name = $data['name'];
        $employee->email = $data['email'];
        $employee->phone = $data['phone'];
        $employee->department_id = $data['department_id'];
        $employee->manager_id = $data['manager_id'];
        $employee->position_id = $data['position_id'];
        $employee->att_profile_id = $data['att_profile_id'];
        $employee->starts_at = $starts_at;
        $employee->user_created_at = $created_at;
        //$employee->user_type = $data['user_type'];
        $employee->admin_show = 1;
        $employee->role_id = $data['role_id'];
        $employee->password = Hash::make($data['password']);
        $employee->dec_password = $data['password'];
        $employee->can_work_home = $data['can_work_home'];
        $employee->max_homeDays_per_week = $data['max_homeDays_per_week'];
        $employee->flexible_home = $data['flexible_home'];
        $employee->can_ex_days = $data['can_ex_days'];
        if(isset($data['home_days'])){
            $data['home_days'] = implode (",", $data['home_days']);
        }else{
            $data['home_days'] = ""    ;
        }
        $employee->home_days =  $data['home_days'];

        $existing_entries = array();
        if(isset($data['copied_managers'])){
	        
	        foreach($data['copied_managers'] as $manager){
	        	$entry = Managers_employee::where('manager_id',$manager)->where('employee_id',$employee->id)->first();
	        	if(!$entry){
	        		$entry = new Managers_employee();
	        	}
	        	$entry->manager_id = $manager;
	        	$entry->employee_id = $employee->id;
	        	$entry->save();
	        	array_push($existing_entries, $entry->manager_id);
	        }
	        
	    }
	    $delete_holidays = Managers_employee::whereNotIn('manager_id',$existing_entries)->where('employee_id',$employee->id)->delete();
        if($employee->save()){
            $data = [];
            $data['status'] = 'success';
            $data['page'] = '/admin/employees';
            $data['msg'] = "Saved Successfully";
            return response()->json(
                        $data
            );  
        }else{
        	$data = [];
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "There was an error";
            return response()->json(
                        $data
            );  
        }
    }
    
    public function delete($id){
       $delete = User::find($id)->delete();
    }
}
