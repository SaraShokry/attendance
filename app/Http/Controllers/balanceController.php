<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Balance;
use Auth;
use App\User;
use DB;

class balanceController extends Controller
{
    public function __construct(){
		$this->logged_user = Auth::user();
        
	}
    public function index() {
        $balances = 
        DB::table('balance')
        ->join('users', 'users.id', '=', 'balance.emp_id')
        ->where('users.office_id',$this->logged_user->office_id)
        ->select('balance.*','users.name')
        ->get();

        
        $data = [];
        $data['partialView'] = 'balance.list';
        $data['balances'] = $balances;
        
        return view('balance.base', $data);
    }

    

   

    public function init(){
    	$balance = new Balance();
    	$balance->save();
    	return redirect('/admin/balance/'.$balance->id.'/edit');
        
    }

    public function edit($id){
    	$balance =  Balance::find($id);
    	
        $data = [];
       

        $employees = DB::table('users')
        ->join('att_profiles', 'users.att_profile_id', '=', 'att_profiles.id')
        ->where('users.office_id',$this->logged_user->office_id)
        ->select('users.*','att_profiles.week_start_day')
        ->get();
        
        $data['employees'] = $employees;
        $data['balance'] = $balance;
        $data['partialView'] = 'balance.form';

        return view('balance.base', $data);
    }
    

    public function save(Request $request){
        $data = $request->input();
        $balance = Balance::find($data['id']);
        if($data['type'] == "Emergency Leaves"){
            $balance->year = $data['emergency_year'];
        }
        if($data['type'] == "Normal Leaves"){
            $balance->year = $data['normal_year'];
        }
        if($data['type'] == "Permissions"){
            $balance->month = $data['month'];
        }
        if($data['type'] == "Home"){
            $dates = explode("-", $data['week']);
            $balance->date_from = date("Y-m-d",strtotime($dates[0]));
            $balance->date_to = date("Y-m-d",strtotime($dates[1]));
        }
        if($data['operation'] == "Subtract"){
            $data['quantity'] = "-".$data['quantity'];
        }
        $balance->quantity = $data['quantity'];
        $balance->type = $data['type'];
        $balance->emp_id = $data['emp_id'];
        $balance->added_by_id = $this->logged_user->id;
        $balance->admin_show = 1;
        if($balance->save()){
            $data = [];
            $data['status'] = 'success';
            $data['page'] = '/admin/balances';
            $data['msg'] = "Saved Successfully";
            return response()->json(
                        $data
            );  
        }else{
        	$data = [];
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "There was an error";
            return response()->json(
                        $data
            );  

        }
  
    }
  
    public function delete($id){
        $count = User::where('balance_id',$id)->count();
        if($count ==0){
            $delete = Balance::find($id)->delete();
        }else{
            abort(404);
        }
    }
}
