<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Helpers\Helpers;
use App\Http\Helpers\SignInOut;




use App\Http\Requests;
use App\Sign_in_out;
use Auth;
use App\Penalty;
use App\Req;
use DB;
use App\User;


class sign_inController extends Controller
{
	public function __construct(){
		$this->logged_user = Auth::user();
        $this->normal_profile =$this->logged_user->getProfile; 

        date_default_timezone_set($this->logged_user->getOffice->time_zone);
		$this->date = date("Y-m-d");
        $this->dateTime = date("Y-m-d H:i:s");
        
    }
    public function sign_in(){

        
        $sign_in = SignInOut::sign_in("now",$this->logged_user->id);

        
        $data = [];
        $data['status'] = $sign_in['status'];
        if(isset($sign_in['page'])){
            $data['page'] = $sign_in['page'];
        }
        if(isset($sign_in['confirm'])){
            $data['confirm'] = $sign_in['confirm'];
        }
        if(isset($sign_in['confirm_msg'])){
            $data['confirm_msg'] = $sign_in['confirm_msg'];
        }
        if(isset($sign_in['route'])){
            $data['route'] = $sign_in['route'];
        }
        if(isset($sign_in['msg'])){
            $data['msg'] = $sign_in['msg'];
        }
        return response()->json(
                    $data
        );
        
    }

    public function sign_out(){
        $sign_out = SignInOut::sign_out("now",$this->logged_user->id);

        $data = [];
        $data['status'] = $sign_out['status'];
        if(isset($sign_out['page'])){
            $data['page'] = $sign_out['page'];
        }
        if(isset($sign_out['confirm'])){
            $data['confirm'] = $sign_out['confirm'];
        }
        if(isset($sign_out['confirm_msg'])){
            $data['confirm_msg'] = $sign_out['confirm_msg'];
        }
        if(isset($sign_out['route'])){
            $data['route'] = $sign_out['route'];
        }
        if(isset($sign_out['msg'])){
            $data['msg'] = $sign_out['msg'];
        }
        return response()->json(
                    $data
        );
    }
    public function dismissWorkFromHome(){
       $home_record = Sign_in_out::where('emp_id',$this->logged_user->id)->orderBy('id','desc')->first(); 
       $home_record->from_home =0;
       $home_record->save();

       $work_from_home_request = Helpers::isThisDate($this->date,"Accepted","Home",$this->logged_user->id);
       if($work_from_home_request){
            $request = Req::where('emp_id',$this->logged_user->id)->where('type','Home')->where('req_day',$this->date)->first();
            $request->status = 0;
            $request->save();
       }
    }

    public function revertSignOut(){
        $sign_out = Sign_in_out::where('emp_id',$this->logged_user->id)->orderBy('id','desc')->first(); 
       $sign_out->sign_out_time =Null;
       $sign_out->save();

    }

    public function refreshSignInOut(){

        $buttons = Helpers::signInOutButtonStatus($this->logged_user->id,$this->dateTime);
        return $buttons;
    }

    

    
}
