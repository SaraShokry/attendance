<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use App\Holiday;
use App\Office;
use App\Holiday_profile;
use App\Holidays_profile;
use DB;
use App\Att_profile;

class holiday_profileController extends Controller
{
    public function __construct(){
		$this->logged_user = Auth::user();

	}
    public function index() {
        if($this->logged_user->office_id !=0){
            $holiday_profiles = Holiday_profile::where('admin_show',1)->where('company_id',$this->logged_user->company_id)->where('office_id',$this->logged_user->office_id)->get();
        }else{
            $holiday_profiles = Holiday_profile::where('admin_show',1)->where('company_id',$this->logged_user->company_id)->get();
        }
    	
    	
        $data = [];
        $data['partialView'] = 'holiday_profiles.list';
        $data['holiday_profiles'] = $holiday_profiles;
        
        return view('holiday_profiles.base', $data);
    }

    public function init(){
    	$holiday_profile = new Holiday_profile();
    	$holiday_profile->save();
    	return redirect('/admin/holiday_profiles/'.$holiday_profile->id.'/edit');
        
    }

    public function edit($id){
    	$holiday_profile =  Holiday_profile::find($id);
    	$holiday_profile->save();
        $data = [];
       
        $offices = Office::where('admin_show',1)->where('company_id',$this->logged_user->company_id)->orderBy('name')->get();
        $entriess = Holidays_profile::where('holiday_profile_id',$holiday_profile->id)->get();
        //$entries = DB::select("select holiday_id from holidays_profiles where holiday_profile_id = ".$holiday_profile->id);
        $entries = [];
        foreach($entriess as $entry){
        	array_push($entries, $entry->holiday_id);
        }
        
        if($this->logged_user->office_id !=0){
        	$holidays = Holiday::where('admin_show',1)->where('company_id',$this->logged_user->company_id)->where('office_id',$this->logged_user->office_id)->orderBy('start_date')->get();
        }else{
        	$holidays = Holiday::where('admin_show',1)->where('company_id',$this->logged_user->company_id)->orderBy('start_date')->get();
        }
        $data['offices'] = $offices;
        $data['entries'] = $entries;
        $data['holidays'] = $holidays;
        $data['holiday_profile'] = $holiday_profile;
        $data['partialView'] = 'holiday_profiles.form';
        return view('holiday_profiles.base', $data);
    }
    

    public function save(Request $request){
        $data = $request->input();

        $holiday_profile = Holiday_profile::find($data['id']);
        $holiday_profile->name = $data['name'];
        $holiday_profile->company_id = $data['company_id'];
        $holiday_profile->office_id = $data['office_id'];
        $holiday_profile->admin_show = 1;
        $holiday_profile->save();
        $existing_entries = array();
        foreach($data['holidays'] as $holiday){
        	$entry = Holidays_profile::where('holiday_id',$holiday)->where('holiday_profile_id',$holiday_profile->id)->first();
        	if(!$entry){
        		$entry = new Holidays_profile();
        	}
        	$entry->holiday_id = $holiday;
        	$entry->holiday_profile_id = $holiday_profile->id;
        	$entry->save();
        	array_push($existing_entries, $entry->holiday_id);
        }
        
        $delete_holidays = Holidays_profile::whereNotIn('holiday_id',$existing_entries)->where('holiday_profile_id',$holiday_profile->id)->delete();
        
        if($holiday_profile->save()){
            $data = [];
            $data['status'] = 'success';
            $data['page'] = '/admin/holiday_profiles';
            $data['msg'] = "Saved Successfully";
            return response()->json(
                        $data
            );  
        }else{
        	$data = [];
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "There was an error";
            return response()->json(
                        $data
            );  

        }
        
    }
    
    public function delete($id){
        $count = Att_profile::where('holiday_profile_id',$id)->count();
        if($count ==0){
            $delete = Holiday_profile::find($id)->delete();
            $delete = Holidays_profile::where('holiday_profile_id',$id)->delete();
        }else{
            abort(404);
        }
       
    }
}
