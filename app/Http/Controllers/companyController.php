<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Company;

class companyController extends Controller
{
    public function index() {
    	$companies = Company::where('admin_show',1)->get();
    	
        $data = [];
        $data['partialView'] = 'companies.list';
        $data['companies'] = $companies;
        
        return view('companies.base', $data);
    }

    

   

    public function init(){
    	$company = new Company();
    	$company->save();
        $data = [];
       
        
        $data['company'] = $company;
        $data['partialView'] = 'companies.form';
        return view('companies.base', $data);
    }

    public function edit($id){
    	$company =  Company::find($id);
    	$company->save();
        $data = [];
       
        
        $data['company'] = $company;
        $data['partialView'] = 'companies.form';
        return view('companies.base', $data);
    }
    

    public function save(Request $request){
        $data = $request->input();
        $company = Company::find($data['id']);
        $company->name = $data['name'];
        $company->admin_show = 1;
        if($company->save()){
            $data = [];
            $data['status'] = 'success';
            $data['page'] = '/admin/companies';
            $data['msg'] = "Saved Successfully";
            return response()->json(
                        $data
            );  
        }else{
        	$data = [];
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "There was an error";
            return response()->json(
                        $data
            );  

        }
        
        

        
        
    }
    
    

    public function delete($id){
       $delete = Company::find($id)->delete();
    }
}
