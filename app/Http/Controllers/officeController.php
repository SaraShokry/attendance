<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Office;
use App\Company;
use App\User;
use Auth;
use DateTimeZone;
use DateTime;
use App\Department;
use App\Position;
use App\Att_profile;
use App\Holiday_profile;
use App\Holiday;

class officeController extends Controller
{
    public function __construct(){
        
        $this->logged_user = Auth::user();

    }
    public function index() {
        if($this->logged_user->office_id !=0){
            $offices = Office::where('admin_show',1)->where('company_id',$this->logged_user->company_id)->where('office_id',$this->logged_user->office_id)->get();
        }else{
            $offices = Office::where('admin_show',1)->where('company_id',$this->logged_user->company_id)->get();
        }
    	
        $data = [];
        $data['partialView'] = 'offices.list';
        $data['offices'] = $offices;
        
        return view('offices.base', $data);
    }

    

   

    public function init(){
    	$office = new Office();
    	$office->save();
    	return redirect('/admin/offices/'.$office->id.'/edit');
    }

    public function edit($id){
        
    	$office =  Office::find($id);
    	$office->save();
    	$companies = Company::where('admin_show',1)->orderBy('name')->get();
        $timezones = DateTimeZone::listIdentifiers(DateTimeZone::ALL);
        $list_items = array();
        foreach ($timezones as $timezone) {
            
            $dtz = new DateTimeZone($timezone);
            $time_in_sofia = new DateTime('now', $dtz);
           
            $offset = $dtz->getOffset( $time_in_sofia )/60/60;
            if($offset > 0){
                $offset = "+".$offset;
            }
            if($offset == 0){
                $offset = "";
            }
            array_push($list_items, ['name' => $timezone,'offset' =>$offset]);
        }

        $data = [];
        $data['list_items'] = $list_items;
        
        $data['companies'] = $companies;
        $data['office'] = $office;
        $data['partialView'] = 'offices.form';
        return view('offices.base', $data);
    }
    

    public function save(Request $request){
        $data = $request->input();
        //Validations
        $check_hq = Office::where('head_quarter',1)->where('company_id',$data['company_id'])->where('id','!=',$data['id'])->count();
        if($check_hq !=0 && $data['head_quarter'] == 1){
            $data = [];
            $data['status'] = 'error';
            
            $data['msg'] = "This company already has a head quarter";
            return response()->json(
                        $data
            );
        }
        $office = Office::find($data['id']);
        $office->company_id = $data['company_id'];
        $office->name = $data['name'];
        $office->time_zone = $data['time_zone'];
        $office->address = $data['address'];
        $office->head_quarter = $data['head_quarter'];
        $office->admin_show = 1;
        
        if($office->save()){
            $data = [];
            $data['status'] = 'success';
            $data['page'] = '/admin/offices';
            $data['msg'] = "Saved Successfully";
            return response()->json(
                        $data
            );  
        }else{
        	$data = [];
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "There was an error";
            return response()->json(
                        $data
            );  

        }
        
        

        
        
    }
    
    

    public function delete($id){
        $count = User::where('office_id',$id)->count();
        $count2 = Department::where('office_id',$id)->count();
        $count3 = Holiday::where('office_id',$id)->count();
        $count4 = Holiday_profile::where('office_id',$id)->count();
        $count5 = Att_profile::where('office_id',$id)->count();
        $count6 = Position::where('office_id',$id)->count();
        $count = $count + $count2 +$count3+$count4+$count5+$count6;

        if($count ==0){
            $delete = Office::find($id)->delete();
        }else{
            abort(404);
        }
       
    }
}
