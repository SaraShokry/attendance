<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;

class userController extends Controller
{
    public function index(){
    	if(Auth::user()){
	    	if (Auth::user()->user_type == "Admin"){
	    		return redirect('/admin/companies');
	    	}elseif(Auth::user()->role_id == 1){
	    		return redirect('/admin/offices');
	    	}elseif(Auth::user()->role_id == 2){
	    		return redirect('/admin/att_profiles');
	    	}elseif(Auth::user()->role_id == 3){
	    		return redirect('/admin/reqs/Permissions');
	    	}elseif(Auth::user()->role_id == 4){
	    		return redirect('/admin/requests/Permissions');
	    	}
	    }else{
	    	return redirect('/login');
	    }
    }

    
}
