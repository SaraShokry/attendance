<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Att_profile;
use App\Office;
use App\Holiday_profile;
use Auth;
use App\User;
use App\Http\Helpers\Helpers;

class att_profileController extends Controller
{
    public function __construct(){
		$this->logged_user = Auth::user();

	}
    public function index() {
    	if($this->logged_user->office_id !=0){
    		$att_profiles = Att_profile::where('admin_show',1)->where('company_id',$this->logged_user->company_id)->where('office_id',$this->logged_user->office_id)->get();
    	}else{
    		$att_profiles = Att_profile::where('admin_show',1)->where('company_id',$this->logged_user->company_id)->get();
    	}
    	
        $data = [];
        $data['partialView'] = 'att_profiles.list';
        $data['att_profiles'] = $att_profiles;
        
        return view('att_profiles.base', $data);
    }

    

   

    public function init(){
    	$att_profile = new Att_profile();
    	$att_profile->save();
    	return redirect('/admin/att_profiles/'.$att_profile->id.'/edit');
        
    }

    public function edit($id){
    	$att_profile =  Att_profile::find($id);
    	$att_profile->save();
        $data = [];
        
        $offices = Office::where('admin_show',1)->where('company_id',$this->logged_user->company_id)->orderBy('name')->get();
        
        if($this->logged_user->office_id !=0){
    		
    		$holiday_profiles = Holiday_profile::where('admin_show',1)->where('company_id',$this->logged_user->company_id)->where('office_id',$this->logged_user->office_id)->orderBy('name')->get();
            $half_profiles = Att_profile::where('admin_show',1)->where('company_id',$this->logged_user->company_id)->where('office_id',$this->logged_user->office_id)->whereIn('profile_type',array('1st_half','2nd_half'))->orderBy('name')->get();
            $work_on_holiday_profiles = Att_profile::where('admin_show',1)->where('company_id',$this->logged_user->company_id)->where('office_id',$this->logged_user->office_id)->where('profile_type','Holiday')->orderBy('name')->get();


    	}else{
    		$holiday_profiles = Holiday_profile::where('admin_show',1)->where('company_id',$this->logged_user->company_id)->orderBy('name')->get();
            $half_profiles = Att_profile::where('admin_show',1)->where('company_id',$this->logged_user->company_id)->whereIn('profile_type',array('1st_half','2nd_half'))->orderBy('name')->get();
            $work_on_holiday_profiles = Att_profile::where('admin_show',1)->where('company_id',$this->logged_user->company_id)->where('profile_type','Holiday')->orderBy('name')->get();
    	}
        $data['offices'] = $offices;
        $data['holiday_profiles'] = $holiday_profiles;
        $data['half_profiles'] = $half_profiles;
        $data['work_on_holiday_profiles'] = $work_on_holiday_profiles;
        $data['att_profile'] = $att_profile;
        $data['partialView'] = 'att_profiles.form';
        return view('att_profiles.base', $data);
    }
    

    public function save(Request $request){
        $data = $request->input();

        if(isset($data['weekends'])){
            $data['weekends'] = implode (",", $data['weekends']);
        }else{
            $data['weekends']= "";
        }
        if($data['sign_in_req'] == 1){
        // Validate Major tardiness time is not exceeding the start of the next day to avoid days overlap
        $major_tardiness_interval = $data['major_tardiness_range'];
        $minor_tardiness_interval = $data['minor_tardiness_range'];
        $sign_in_end_time = $data['sign_in_end_time'];
        $sign_in_start_time = $data['sign_in_start_time'];
        $sign_in_permissibility = $data['time_allowed_before_sign_in'];
        $sign_in_considering_permissiblity_start_time = Helpers::addIntervalToTime($sign_in_start_time,$sign_in_permissibility,"Subtract");
        $sign_in_dateTimes =  Helpers::timesToDateTimes($sign_in_start_time,$sign_in_end_time,"1989-06-18");
        $sign_in_dateTime = $sign_in_dateTimes['datetime1'];
        $sign_in_interval =  Helpers::addDates($sign_in_dateTimes['datetime1'],$sign_in_dateTimes['datetime2'],"subtract");
        $major_tardines_time = Helpers::addIntervalToDate($sign_in_dateTime,$sign_in_interval,"Add");
        $major_tardines_time = Helpers::addIntervalToDate($major_tardines_time,$minor_tardiness_interval,"Add");
        $major_tardines_time = Helpers::addIntervalToDate($major_tardines_time,$major_tardiness_interval,"Add");
        $next_sign_in_start_interval = date("Y-m-d H:i:s",strtotime($sign_in_dateTime." +1 day"));
        $next_sign_in_start_interval = Helpers::addIntervalToDate($next_sign_in_start_interval,$sign_in_permissibility,"subtract");
        
        if(strtotime($next_sign_in_start_interval)<strtotime($major_tardines_time)){
            $data = [];
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "The major tardiness time can't exceed the sign in with permissibility limit";
            return response()->json(
                        $data
            );     
        }

        
        
        // Validate EOD is not before the calculated Work Day
        
        $end_of_day = $data['end_of_day'];
        $workday_hours = $data['work_hours'];
        $sign_in_EOD_dateTimes =  Helpers::timesToDateTimes($sign_in_end_time, $end_of_day,"1989-06-18");
        $calculated_workday = Helpers::addIntervalToDate($sign_in_EOD_dateTimes['datetime1'],$workday_hours,"Add");
        $end_of_day_date = $sign_in_EOD_dateTimes['datetime2'];

        if(strtotime($end_of_day_date)<strtotime($calculated_workday)){
            $data = [];
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "The Required Worday Hours Will Exceed Maximum Sign Out Time ";
            return response()->json(
                        $data
            );     
        }
        ////
        // Validate Sign In Range doesn't exceed the Maximum Sign Out Time
        
        if(strtotime($end_of_day_date)<strtotime($major_tardines_time)){
            $data = [];
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "The Allowed Sign In Range Exceeds Sign Out Time ";
            return response()->json(
                        $data
            );     
        }
        
        // Validate Sign In Range doesn't exceed the Maximum Sign Out Time
        
        if(strtotime($calculated_workday)<strtotime($major_tardines_time)){
            $data = [];
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "The Allowed Sign In Range Exceeds The Assumed End of Workday";
            return response()->json(
                        $data
            );     
        }
        }
        $data['admin_show'] = 1;
        if(isset($data['update'])){
            unset($data['update']);
            $att_profile = Att_profile::find($data['id']);
            $att_profile->update($data);
        }
        if(isset($data['new'])){

            unset($data['new']);
            
            
            $att_profile = new Att_profile();
            $att_profile->save();
            $data['id'] = $att_profile->id;
            $att_profile->update($data);
        }
        
        if($att_profile->save()){
            $data = [];
            $data['status'] = 'success';
            $data['page'] = '/admin/att_profiles';
            $data['msg'] = "Saved Successfully";
            return response()->json(
                        $data
            );  
        }else{
        	$data = [];
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "There was an error";
            return response()->json(
                        $data
            );  

        }
        
        

        
        
    }
    
    

    public function delete($id){
    	$employees = User::where('att_profile_id',$id)->count();
    	if($employees ==0){
    		$delete = Att_profile::find($id)->delete();
    	}else{
    		abort(404);
    	}
    }
    	
       
}
