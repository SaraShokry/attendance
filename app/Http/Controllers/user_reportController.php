<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use App\Req;
use DB;
use App\Sign_in_out;
use App\Penalty;

class user_reportController extends Controller
{
    public function __construct(){
        
        $this->logged_user = Auth::user();
        date_default_timezone_set($this->logged_user->getOffice->time_zone);

    }
    public function index($type) {

    	$data = [];
        if($type == "attendance"){
        	$sign_ins = Sign_in_out::where('emp_id',$this->logged_user->id)->get();
        	$data['sign_ins'] = $sign_ins;
        }
        elseif($type == "penalties"){
        	$now = date("Y-m-d H:i:s");
    		$now_minus_one_hour = date("Y-m-d H:i:s",strtotime($now." -1 hour"));
        	$penalties = Penalty::where('emp_id',$this->logged_user->id)->where('penalty_created_at','<=',$now_minus_one_hour)->get();
        	$data['penalties'] = $penalties;
        }
        
        
        
        $data['partialView'] = 'user_reports.list';
        
        $data['type'] = $type;
        return view('user_reports.base', $data);
    }
}
