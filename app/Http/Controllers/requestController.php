<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Office;
use Auth;
use App\Http\Helpers\Helpers;
use App\Req;
use App\Sign_in_out;
use App\Balance;

class requestController extends Controller
{
    public function __construct(){
        
        $this->logged_user = Auth::user();
        date_default_timezone_set($this->logged_user->getOffice->time_zone);

        

    }
    public function index($type) {
    	$data = [];
        if($type =="Permissions"){
            $data['partialView'] = 'requests.permission_form';
        }
        if($type =="Normal Leaves"){
            $data['partialView'] = 'requests.normal_form';
        }
        if($type =="Emergency Leaves"){
           $data['partialView'] = 'requests.emergency_form'; 
        }
        if($type =="Home"){
           $data['partialView'] = 'requests.home_form'; 
        }
        if($type =="Half Day"){
           $data['partialView'] = 'requests.half_form'; 
        }
        
        $requests = Office::get();

    	
        
        
        $data['requests'] = $requests;
        $data['type'] = $type;
        return view('requests.base', $data);
    }

    

   

    public function init(){
    	$office = new Office();
    	$office->save();
    	$companies = Company::where('admin_show',1)->orderBy('name')->get();
        
        $data = [];
       
        $data['companies'] = $companies;
        $data['office'] = $office;
        $data['partialView'] = 'requests.form';
        return view('requests.base', $data);
    }

    public function edit($id){
    	$office =  Office::find($id);
    	$office->save();
    	$companies = Company::where('admin_show',1)->orderBy('name')->get();
        $timezones = DateTimeZone::listIdentifiers(DateTimeZone::ALL);
        $list_items = array();
        foreach ($timezones as $timezone) {
            
            $dtz = new DateTimeZone($timezone);
            $time_in_sofia = new DateTime('now', $dtz);
           
            $offset = $dtz->getOffset( $time_in_sofia )/60/60;
            if($offset > 0){
                $offset = "+".$offset;
            }
            if($offset == 0){
                $offset = "";
            }
            array_push($list_items, ['name' => $timezone,'offset' =>$offset]);
        }

        $data = [];
        $data['list_items'] = $list_items;
        $data['companies'] = $companies;
        $data['office'] = $office;
        $data['partialView'] = 'requests.form';
        return view('requests.base', $data);
    }
    
    
    public function saveExchangeHome(Request $request){
        $data = $request->input();

        $weekend_holiday = Helpers::isThisRequestHolidayWeekend($this->logged_user->id,$data['home_day']);
        if($weekend_holiday){
            $data = [];
            $data['status'] = 'error';
            
            $data['msg'] = "You are trying to request a work from home day on a day off";
            return response()->json(
                        $data
            );    
        }
        //Default work from home days Validations
        $home_day = $data['home_day'];
        $req_day_name = date("l",strtotime($home_day));
        $default_work_from_home_days = explode(",",$this->logged_user->home_days);

        if(in_array($req_day_name, $default_work_from_home_days)){
        	$data = [];
            $data['status'] = 'error';
            
            $data['msg'] = "You cant not choose ".$req_day_name;
            return response()->json(
                        $data
            );
        }
        $req_day_order = date("N",strtotime($data['home_day']));
        $req_day_order=$req_day_order+2;
        if($req_day_order >7){
        	$req_day_order = $req_day_order -7;	
        }
        
        $req_day_week_offset = $req_day_order - $this->logged_user->getProfile->week_start_day;
        if($req_day_week_offset<0){
        	$req_day_week_offset = $req_day_week_offset +7;
        }
    	$week_start_date = date("Y-m-d",strtotime($home_day."- ".$req_day_week_offset." days"));
    	$week_end_date = date("Y-m-d",strtotime($week_start_date."+ 6 days"));
    	$previous_work_from_homes = Req::where('emp_id',$this->logged_user->id)->where('type','Home')->whereIn('status',array(1,2))->where('req_day','>=',$week_start_date)->where('req_day','<=',$week_end_date)->count();//Pending + Accepted
        $balance_added = Balance::where('emp_id',$this->logged_user->id)->where('type','Home')->where('date_from',$week_start_date)->where('date_to',$week_end_date)->sum('quantity');
    	$total_to_check_against = $previous_work_from_homes + $balance_added +1;
    	if($total_to_check_against > $this->logged_user->max_homeDays_per_week){
        	$data = [];
            $data['status'] = 'error';
            
            $data['msg'] = "You have exceeded your work from home quota for this week";
            return response()->json(
                        $data
            );

        }
        
        // validate intersecting requests
        $intersecting_requests = Helpers::validateIntersectingDayRequests($this->logged_user->id,$home_day,"Home");
        if(!$intersecting_requests){
            $data = [];
            $data['status'] = 'error';
            
            $data['msg'] = "This request conflicts with the interval of a previous request";
            return response()->json(
                        $data
            );

        }

        // validate exchanged day was not previously taken
        $weekLimits = Helpers::getWeekOfThis($data['home_day'],$this->logged_user->id);
        $week_start_date = $weekLimits['start_date'];
        $day_date = Helpers::moveUntillDayName($week_start_date,$data['instead']);
        
        $already_taken = Sign_in_out::where("date",$day_date)->where("emp_id",$this->logged_user->id)->where('from_home',1)->count();
        if($already_taken >0){
            $data = [];
            $data['status'] = 'error';
            
            $data['msg'] = "The day you are trying to exchange has been previously taken this week";
            return response()->json(
                        $data
            );

        }


        $req = new Req();
        $req->emp_id = $this->logged_user->id;
        $req->type = "Home";
        $req->req_day = date("Y-m-d",strtotime($data['home_day']));
        $req->add_info = $data['instead'];
        
        
        
        if($req->save()){
            Helpers::sendReqEmail($req);
            $data = [];
            $data['status'] = 'success';
            $data['page'] = 'none';
            $data['msg'] = "Saved Successfully";
            return response()->json(
                        $data
            );  
        }else{
        	$data = [];
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "There was an error";
            return response()->json(
                        $data
            );  

        }
        
        

        
        
    }
	public function saveFlexHome(Request $request){
        $data = $request->input();

        $weekend_holiday = Helpers::isThisRequestHolidayWeekend($this->logged_user->id,$data['home_day']);
        if($weekend_holiday){
            $data = [];
            $data['status'] = 'error';
            
            $data['msg'] = "You are trying to request a work from home day on a day off";
            return response()->json(
                        $data
            );    
        }
        //Week quota exceeded Validations
        $home_day = $data['home_day'];
        $req_day_order = date("N",strtotime($home_day));

        $req_day_order=$req_day_order+2;
        if($req_day_order >7){
        	$req_day_order = $req_day_order -7;	
        }
        
        $req_day_week_offset = $req_day_order - $this->logged_user->getProfile->week_start_day;
        if($req_day_week_offset<0){
        	$req_day_week_offset = $req_day_week_offset +7;
        }
    	$week_start_date = date("Y-m-d",strtotime($home_day."- ".$req_day_week_offset." days"));
    	$week_end_date = date("Y-m-d",strtotime($week_start_date."+ 6 days"));
    	$previous_work_from_homes = Req::where('emp_id',$this->logged_user->id)->where('type','Home')->whereIn('status',array(1,2))->where('req_day','>=',$week_start_date)->where('req_day','<=',$week_end_date)->count();//Pending + Accepted
        $balance_added = Balance::where('emp_id',$this->logged_user->id)->where('type','Home')->where('date_from',$week_start_date)->where('date_to',$week_end_date)->sum('quantity');
    	$total_to_check_against = $previous_work_from_homes + $balance_added+1;
    	if($total_to_check_against > $this->logged_user->max_homeDays_per_week){
        	$data = [];
            $data['status'] = 'error';
            
            $data['msg'] = "You have exceeded your work from home quota for this week";
            return response()->json(
                        $data
            );

        }
        
        $from = strtotime("18-06-1989 ".$data['permission_from']);
        
        
        
        // validate intersecting requests
        $intersecting_requests = Helpers::validateIntersectingDayRequests($this->logged_user->id,$home_day,"Home");
        if(!$intersecting_requests){
            $data = [];
            $data['status'] = 'error';
            
            $data['msg'] = "This request conflicts with the interval of a previous request";
            return response()->json(
                        $data
            );

        }
        
        
        
        $req = new Req();
        $req->emp_id = $this->logged_user->id;
        $req->type = "Home";
        $req->req_day = date("Y-m-d",strtotime($data['home_day']));
        
        if($req->save()){
            Helpers::sendReqEmail($req);
            $data = [];
            $data['status'] = 'success';
            $data['page'] = 'none';
            $data['msg'] = "Saved Successfully";
            return response()->json(
                        $data
            );  
        }else{
        	$data = [];
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "There was an error";
            return response()->json(
                        $data
            );  

        }
        
        

        
        
    }
    public function savePermission(Request $request){

        $data = $request->input();

        //Start End Validations
        $to = strtotime($data['permission_to']);
        $from = strtotime($data['permission_from']);
        if($to<$from){
        	$data = [];
            $data['status'] = 'error';
            
            $data['msg'] = "The end time is before the start time";
            return response()->json(
                        $data
            );

        }
        $remaining_permissions_this_day_in_minutes = Helpers::getRemainingPermissions($this->logged_user->id,$data['permission_from'],"day");
        $remaining_permissions_this_month_in_minutes = Helpers::getRemainingPermissions($this->logged_user->id,$data['permission_from'],"month");
        if($remaining_permissions_this_day_in_minutes == Null){
            $data = [];
            $data['status'] = 'error';
            
            $data['msg'] = "You are requesting a permission out of the range of your normal workday";
            return response()->json(
                        $data
            );    
        }
        // validate weekend or holidays
        $weekend_holiday = Helpers::isThisRequestHolidayWeekend($this->logged_user->id,$data['permission_from']);
        if($weekend_holiday){
            $data = [];
            $data['status'] = 'error';
            
            $data['msg'] = "You are trying to request a permission on a day off";
            return response()->json(
                        $data
            );    
        }
        //permission unit validation

        $diff = $to - $from;
        $diff_mins = $diff/60;
        $permission_unit = $this->logged_user->getProfile->min_permission_unit;
        $permission_unit_mins = Helpers::timeToSeconds($permission_unit)/60;
        if($permission_unit_mins > 0){
            if(!is_int($diff_mins/$permission_unit_mins)){
                $data = [];
                $data['status'] = 'error';
                
                $data['msg'] = "The duration for your permission must be in multiples of ".$permission_unit_mins." minutes";
                return response()->json(
                            $data
                );
            }
        }
        //Same day Validations
        $current_interval_in_minutes = ($to - $from)/60;
        if($current_interval_in_minutes>$remaining_permissions_this_day_in_minutes){
        	$data = [];
            $data['status'] = 'error';
            
            $data['msg'] = "You have exceeded the maximum permission duration acceptable per day";
            return response()->json(
                        $data
            );

        }
        //Exceeding monthly permissions Validations
        
        if($current_interval_in_minutes>$remaining_permissions_this_month_in_minutes){
        	$data = [];
            $data['status'] = 'error';
            
            $data['msg'] = "You only have ".$remaining_permissions_this_month_in_minutes." permission minutes remaining this month";
            return response()->json(
                        $data
            );

        }
        // validate intersectiong requests
        $intersecting_requests = Helpers::validateIntersectingIntervalsRequests($this->logged_user->id,$data['permission_from'],$data['permission_to'],"Permission");
        
        if(!$intersecting_requests){
            $data = [];
            $data['status'] = 'error';
            
            $data['msg'] = "This request conflicts with the interval of a previous request";
            return response()->json(
                        $data
            );    
        }


        $req_day = Helpers::getDayLimits($data['permission_from'],$this->logged_user->id,"normal");
        $req_day = $req_day['start_of_day'];
        $req = new Req();
        $req->emp_id = $this->logged_user->id;
        $req->type = "Permissions";
        $req->req_day = $req_day;
        $req->time_from = $data['permission_from'];
        $req->time_to = $data['permission_to'];
        $req->actual_time_from = $data['permission_from'];
        $req->actual_time_to = $data['permission_to'];
        $req->interval_in_time = Helpers::subtractDates($data['permission_to'],$data['permission_from']);
        
        if($req->save()){
            
            Helpers::sendReqEmail($req);
            $data = [];
            $data['status'] = 'success';
            $data['page'] = 'none';
            $data['msg'] = "Saved Successfully";
            return response()->json(
                        $data
            );  
        }else{
        	$data = [];
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "There was an error";
            return response()->json(
                        $data
            );  

        }
        
        

        
        
    }
    
    public function saveHalfDay(Request $request){
        $data = $request->input();

        $req_half_day = strtotime($data['half_day']." ".$this->logged_user->getProfile->sign_in_end_time);
        $weekend_holiday = Helpers::isThisRequestHolidayWeekend($this->logged_user->id,$data['half_day']);
        if($weekend_holiday){
            $data = [];
            $data['status'] = 'error';
            
            $data['msg'] = "You are trying to request a half day on a day off";
            return response()->json(
                        $data
            );    
        }
        //Cutt-off time Validations
        $now = strtotime(date("Y-m-d H:i:s"));
        
        $required_cuttoff = $req_half_day - $now ;
        $cuttoff_time =$this->logged_user->getProfile->normal_leave_cuttoff;
        $parts = explode(":",$cuttoff_time);
        $hours = $parts[0];
        $mins = $parts[1];
        $secs = $parts[2];
        $real_cuttoff = $secs + $mins*60 + $hours*60*60;

        if($required_cuttoff<$real_cuttoff){
        	$data = [];
            $data['status'] = 'error';
            
            $data['msg'] = "You must request a half day leave at least ".$hours .":".$mins." hours prior to the requested day";
            return response()->json(
                        $data
            );

        }
        //Exceeding annual leave Validations
        $previous_intervals = 18; //Days
        $current_interval = 0.5;
        $total = $previous_intervals + $current_interval;
        $max_allowed_leaves = $this->logged_user->getProfile->allowed_normal_leaves ;
        $remaining_leaves = $this->logged_user->getProfile->allowed_normal_leaves - $previous_intervals;
        if($total>$max_allowed_leaves){
        	$data = [];
            $data['status'] = 'error';
            
            $data['msg'] = "You only have ".$remaining_leaves." normal leave days remaining this year";
            return response()->json(
                        $data
            );

        }
        // validate intersecting requests
        $intersecting_requests = Helpers::validateIntersectingDayRequests($this->logged_user->id,$data['half_day'],"Half Day");
        if(!$intersecting_requests){
            $data = [];
            $data['status'] = 'error';
            
            $data['msg'] = "This request conflicts with the interval of a previous request";
            return response()->json(
                        $data
            );

        }

        $req = new Req();
        $req->emp_id = $this->logged_user->id;
        $req->type = "Half Day";
        $req->req_day = date("Y-m-d",strtotime($data['half_day']));
        $req->add_info = $data['half'];
        $req->interval_in_days = 0.5;
		
        
        
        
        if($req->save()){
            Helpers::sendReqEmail($req);
            $data = [];
            $data['status'] = 'success';
            $data['page'] = 'none';
            $data['msg'] = "Saved Successfully";
            return response()->json(
                        $data
            );  
        }else{
        	$data = [];
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "There was an error";
            return response()->json(
                        $data
            );  

        }
        
        

        
        
    }
    public function saveNormal(Request $request){
        $data = $request->input();

        //Start End Validations
        $to = strtotime($data['normal_to']);
        $from = strtotime($data['normal_from']);

        if($to<$from){
        	$data = [];
            $data['status'] = 'error';
            
            $data['msg'] = "The end date is before the start date";
            return response()->json(
                        $data
            );

        }
        if($to<$from){
            $data = [];
            $data['status'] = 'error';
            
            $data['msg'] = "The end date is before the start date";
            return response()->json(
                        $data
            );

        }
        //Same year Validation
        $year_to = date("Y",strtotime($data['normal_to']));
        $year_from = date("Y",strtotime($data['normal_from']));
        if($year_to != $year_from){
            $data = [];
            $data['status'] = 'error';
            
            $data['msg'] = "Request period must be in the same year";
            return response()->json(
                        $data
            );
        }
        
        // Check if leave starts on holiday or weekend
        $weekend_holiday = Helpers::isThisRequestHolidayWeekend($this->logged_user->id,$data['normal_from']);
        if($weekend_holiday){
            $data = [];
            $data['status'] = 'error';
            
            $data['msg'] = "You are trying to request a leave on a day off";
            return response()->json(
                        $data
            );    
        }
        $from = strtotime($data['normal_from']." ".$this->logged_user->getProfile->sign_in_end_time);


        //Cutt-off time Validations
        $now = strtotime(date("Y-m-d H:i:s"));
        
        $required_cuttoff = $from - $now ;
        $cuttoff_time =$this->logged_user->getProfile->normal_leave_cuttoff;
        $parts = explode(":",$cuttoff_time);
        $hours = $parts[0];
        $mins = $parts[1];
        $secs = $parts[2];
        $real_cuttoff = $secs + $mins*60 + $hours*60*60;

        if($required_cuttoff<$real_cuttoff){
        	$data = [];
            $data['status'] = 'error';
            
            $data['msg'] = "You must request a normal leave at least ".$hours .":".$mins." hours prior to the start of the leave";
            return response()->json(
                        $data
            );

        }
        
        // validate intersectiong requests
        $intersecting_requests = Helpers::validateIntersectingIntervalsRequests($this->logged_user->id,$data['normal_from'],$data['normal_to'],"Normal Leave");
        
        if(!$intersecting_requests){
            $data = [];
            $data['status'] = 'error';
            
            $data['msg'] = "This request conflicts with the interval of a previous request";
            return response()->json(
                        $data
            );    
        }

		$total_requested_days = Helpers::getCountOfDaysBetween($data['normal_from'],$data['normal_to']);
        $holidays_weekends_with_the_requested_dates = Helpers::getWeekendsHolidaysWithin($data['normal_from'],$data['normal_to'],$this->logged_user->id);
        $net_requested_day_count = $total_requested_days - $holidays_weekends_with_the_requested_dates ;

        //Exceeding annual leaves Validations
        $remaining_leaves = Helpers::getRemainingLeaves($this->logged_user->id,date("Y",strtotime($data['normal_from'])),"Normal",$profile_type="normal");
        
        if($net_requested_day_count>$remaining_leaves){
            $data = [];
            $data['status'] = 'error';
            
            $data['msg'] = "You only have ".$remaining_leaves." normal leave days remaining this year";
            return response()->json(
                        $data
            );

        }
        $profile = Helpers::getProfile($this->logged_user->id,"normal");
        $actual_time_from = date("Y-m-d H:i:s",strtotime($data['normal_from']." ".$profile->sign_in_start_time));
        $dayLimits = Helpers::getDayLimits($data['normal_to']." ".$profile->sign_in_start_time,$this->logged_user->id);
        $actual_time_to = $dayLimits['end_of_day'];
        $time_to = date("Y-m-d H:i:s",strtotime($data['normal_to']." 23:59:59"));
        $req = new Req();
        $req->emp_id = $this->logged_user->id;
        $req->type = "Normal Leaves";
        $req->time_from = date("Y-m-d H:i:s",strtotime($data['normal_from']." 00:00:00"));
        $req->time_to = $time_to;
        $req->actual_time_from = $actual_time_from;
        $req->actual_time_to = $actual_time_to;
        $req->interval_in_days = $net_requested_day_count;
        
        
        if($req->save()){
            Helpers::sendReqEmail($req);
            $data = [];
            $data['status'] = 'success';
            $data['page'] = 'none';
            $data['msg'] = "Saved Successfully";
            return response()->json(
                        $data
            );  
        }else{
        	$data = [];
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "There was an error";
            return response()->json(
                        $data
            );  

        }
        
        

        
        
    }
    public function saveEmergency(Request $request){
        $data = $request->input();

        //Start End Validations
        $to = strtotime($data['emergency_to']);
        $from = strtotime($data['emergency_from']);
        if($to<$from){
        	$data = [];
            $data['status'] = 'error';
            
            $data['msg'] = "The end date is before the start date";
            return response()->json(
                        $data
            );

        }
        //Same year Validation
        $year_to = date("Y",strtotime($data['emergency_to']));
        $year_from = date("Y",strtotime($data['emergency_from']));
        if($year_to != $year_from){
            $data = [];
            $data['status'] = 'error';
            
            $data['msg'] = "Request period must be in the same year";
            return response()->json(
                        $data
            );
        }
        // Check if this leave starts on a hoilday or a weekend
        $weekend_holiday = Helpers::isThisRequestHolidayWeekend($this->logged_user->id,$data['emergency_from']);
        if($weekend_holiday){
            $data = [];
            $data['status'] = 'error';
            
            $data['msg'] = "You are trying to request a leave  on a day off";
            return response()->json(
                        $data
            );    
        }
        //Exceeding monthly Emergency Validations
        $previous_intervals = 3; //Days
        $current_interval = ($to - $from)/60/60/24 +1;
        $total = $previous_intervals + $current_interval;
        $max_allowed_leaves = $this->logged_user->getProfile->allowed_emergency_leaves ;
        $remaining_leaves = $this->logged_user->getProfile->allowed_emergency_leaves - $previous_intervals;
        if($total>$max_allowed_leaves){
        	$data = [];
            $data['status'] = 'error';
            
            $data['msg'] = "You only have ".$remaining_leaves." emergency leave days remaining this year";
            return response()->json(
                        $data
            );

        }
        // check Intersecting requests
        $intersecting_requests = Helpers::validateIntersectingIntervalsRequests($this->logged_user->id,$data['emergency_from'],$data['emergency_to'],"Emergency Leave");

        if(!$intersecting_requests){
            $data = [];
            $data['status'] = 'error';
            
            $data['msg'] = "This request conflicts with the interval of a previous request";
            return response()->json(
                        $data
            );    
        }

        $total_requested_days = Helpers::getCountOfDaysBetween($data['emergency_from'],$data['emergency_to']);
        $total_requested_days = count($total_requested_days);
        $holidays_weekends_with_the_requested_dates = Helpers::getWeekendsHolidaysWithin($data['emergency_from'],$data['emergency_to'],$this->logged_user->id);
        $net_requested_day_count = $total_requested_days - $holidays_weekends_with_the_requested_dates ;

        //Exceeding annual leaves Validations
        $remaining_leaves = Helpers::getRemainingLeaves($this->logged_user->id,date("Y",strtotime($data['emergency_from'])),"Emergency",$profile_type="normal");
        
        if($net_requested_day_count>$remaining_leaves){
            $data = [];
            $data['status'] = 'error';
            
            $data['msg'] = "You only have ".$remaining_leaves." normal leave days remaining this year";
            return response()->json(
                        $data
            );

        }
        $profile = Helpers::getProfile($this->logged_user->id,"normal");
        $actual_time_from = date("Y-m-d H:i:s",strtotime($data['emergency_from']." ".$profile->sign_in_start_time));
        $dayLimits = Helpers::getDayLimits($data['emergency_to']." ".$profile->sign_in_start_time,$this->logged_user->id);
        
        $actual_time_to = $dayLimits['end_of_day'];
        $time_to = date("Y-m-d H:i:s",strtotime($data['emergency_to']." 23:59:59"));

        $req = new Req();
        if($profile->emergency_leave_without_permission == 1){
            $req->status = 1;
        }
        $req->emp_id = $this->logged_user->id;
        $req->type = "Emergency Leaves";
        $req->time_from = date("Y-m-d H:i:s",strtotime($data['emergency_from']));
        $req->time_to = $time_to;
        $req->actual_time_from = $actual_time_from;
        $req->actual_time_to = $actual_time_to;
        $req->interval_in_days = $net_requested_day_count;
        
        
        
        
        if($req->save()){
            Helpers::sendReqEmail($req);
            $data = [];
            $data['status'] = 'success';
            $data['page'] = 'none';
            $data['msg'] = "Saved Successfully";
            return response()->json(
                        $data
            );  
        }else{
        	$data = [];
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "There was an error";
            return response()->json(
                        $data
            );  

        }
        
        

        
        
    }
    
    

    public function delete($id){
        $count = User::where('office_id',$id)->count();
        $count2 = Department::where('office_id',$id)->count();
        $count3 = Holiday::where('office_id',$id)->count();
        $count4 = Holiday_profile::where('office_id',$id)->count();
        $count5 = Att_profile::where('office_id',$id)->count();
        $count6 = Position::where('office_id',$id)->count();
        $count = $count + $count2 +$count3+$count4+$count5+$count6;

        if($count ==0){
            $delete = Office::find($id)->delete();
        }else{
            abort(404);
        }
       
    }

    
}
