<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Position;
use App\Office;
use Auth;
use App\User;

class positionController extends Controller
{
    public function __construct(){
		$this->logged_user = Auth::user();

	}
    public function index() {
        if($this->logged_user->office_id !=0){
            $positions = Position::where('admin_show',1)->where('company_id',$this->logged_user->company_id)->where('office_id',$this->logged_user->office_id)->get();
        }else{
            $positions = Position::where('admin_show',1)->where('company_id',$this->logged_user->company_id)->get();
        }
    	
    	
        $data = [];
        $data['partialView'] = 'positions.list';
        $data['positions'] = $positions;
        
        return view('positions.base', $data);
    }

    

   

    public function init(){
    	$position = new Position();
    	$position->save();
    	return redirect('/admin/positions/'.$position->id.'/edit');
        
    }

    public function edit($id){
    	$position =  Position::find($id);
    	$position->save();
        $data = [];
       
        $offices = Office::where('admin_show',1)->where('company_id',$this->logged_user->company_id)->orderBy('name')->get();
        $data['offices'] = $offices;
        $data['position'] = $position;
        $data['partialView'] = 'positions.form';
        return view('positions.base', $data);
    }
    

    public function save(Request $request){
        $data = $request->input();
        $position = Position::find($data['id']);
        $position->name = $data['name'];
        $position->company_id = $data['company_id'];
        $position->office_id = $data['office_id'];
        $position->admin_show = 1;
        if($position->save()){
            $data = [];
            $data['status'] = 'success';
            $data['page'] = '/admin/positions';
            $data['msg'] = "Saved Successfully";
            return response()->json(
                        $data
            );  
        }else{
        	$data = [];
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "There was an error";
            return response()->json(
                        $data
            );  

        }
        
        

        
        
    }
    
    

    public function delete($id){
        $count = User::where('position_id',$id)->count();
        if($count ==0){
            $delete = Position::find($id)->delete();
        }else{
            abort(404);
        }
       
    }
}
