<?php

namespace App\Http\Controllers;
use App\Http\Helpers\SignInOut;
use App\Http\Helpers\Helpers;
use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use App\Req;
use DB;
use App\Sign_in_out;
use App\Managers_employee;
class inbound_requestController extends Controller
{
    public function __construct(){
        
        $this->logged_user = Auth::user();
        //date_default_timezone_set($this->logged_user->getOffice->time_zone);

    }
    public function index($type) {
        $user_id = $this->logged_user->id;
        // get employees that the logged user is a copy manager for them
        $get_employees_of_manager = Managers_employee::where('manager_id',$this->logged_user->id)->get();
       
        $get_employees_of_manager_array = array();
        foreach($get_employees_of_manager as $employee){
            array_push($get_employees_of_manager_array, $employee->employee_id);
        }
        
        


        $reqs = DB::table('requests')
        ->join('users', 'users.id', '=', 'requests.emp_id')
        
        
        ->where(function ($query) use ($get_employees_of_manager_array,$user_id,$type) {
            $query->whereIn('requests.emp_id',$get_employees_of_manager_array)
                  ->orWhere('users.manager_id',$user_id);
        })->where(function ($query) use ($get_employees_of_manager_array,$user_id,$type) {
            $query->where('requests.status',2)
                  ->where('requests.type',$type);
        })->select('requests.*','users.name')
        ->get();
        
        
        if($type == "Alls"){

            $reqs = DB::table('requests')
        ->join('users', 'users.id', '=', 'requests.emp_id')
        
        
        
        
        ->where(function ($query) use ($get_employees_of_manager_array,$user_id,$type) {
            $query->whereIn('requests.emp_id',$get_employees_of_manager_array)
            ->orWhere('users.manager_id',$user_id);
        })->where(function ($query) use ($get_employees_of_manager_array,$user_id,$type) {
            $query->whereIn('requests.status',array(0,1));
        })->select('requests.*','users.name')
        ->select('requests.*','users.name')
        ->get();

        }

        if($type == "Overtime_report"){
            $reqs = DB::table('requests')
        ->join('users', 'users.id', '=', 'requests.emp_id')
        
        
        ->where(function ($query) use ($get_employees_of_manager_array,$user_id,$type) {
            $query->whereIn('requests.emp_id',$get_employees_of_manager_array)
                  ->orWhere('users.manager_id',$user_id);
        })->where(function ($query) use ($get_employees_of_manager_array,$user_id,$type) {
            $query->where('requests.status',1)
                  ->where('requests.type',"Overtime");
        })->select('requests.*','users.name')
        ->get();

        }
        
        $data = [];
        $data['partialView'] = 'reqs.list';
        $data['reqs'] = $reqs;
        $data['type'] = $type;
        return view('reqs.base', $data);
    }

    public function userRequests($user_id,$status,$taken) {
        $reqs = DB::table('requests')
        ->join('users', 'users.id', '=', 'requests.emp_id')
        ->where('requests.status',$status)
        ->where('users.id',$this->logged_user->id)
        ->where('taken',$taken)
        ->select('requests.*','users.name')
        ->get();
        
        $data = [];
        $data['partialView'] = 'reqs.list';
        $data['reqs'] = $reqs;
        $data['type'] = "All";
        $data['taken'] = $taken;
        $data['status'] = $status;
        return view('reqs.base', $data);
    }

    public function editSignoutInterval(Request $request){
        $data = $request->input();
        $req = Req::find($data['id']);
        $req->interval_in_time = $data['interval_in_time'];
        
        if($req->save()){
            $data = [];
            $data['status'] = 'success';
            $data['page'] = '/admin/reqs/Overtime';
            $data['msg'] = "Saved Successfully";
            return response()->json(
                        $data
            );  
        }else{
            $data = [];
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "There was an error";
            return response()->json(
                        $data
            );  

        }

    }


    public function accept($id) {
        $req = Req::find($id);
        $req->status = 1;
        $req->save();

        Helpers::sendReqEmail($req);

        /*if($req->type == "Permissions"){
            //get the sign in details of this day
            $sign_in_out = Sign_in_out::where('date',$req->req_day)->where('emp_id',$req->emp_id)->first();

            if($sign_in_out){
                // check if the permission start time is inside the sign in range
                $dayLimits = Helpers::getDayLimits($sign_in_out->sign_in_time,$req->emp_id,$sign_in_out->day_type);
                $profile = Helpers::getProfile($sign_in_out->emp_id,$sign_in_out->day_type);
                $start_of_day_with_permissibility = Helpers::addIntervalToDate($dayLimits['start_of_day'],$profile->time_allowed_before_sign_in,"Subtract");
                $sign_in_end_datetime  = Helpers::timesToDateTimes($profile->sign_in_start_time,$profile->sign_in_end_time,date('Y-m-d'),strtotime($dayLimits['start_of_day']));
                $sign_in_end_datetime = $sign_in_end_datetime['datetime2'];
                if(strtotime($req->time_from) >= strtotime($start_of_day_with_permissibility) && strtotime($req->time_from) <= strtotime($sign_in_end_datetime)){
                    // subtract the permission interval from the sign in time
                    $new_sign_in_time =  Helpers::addIntervalToDate($sign_in_out->sign_in_time,$req->interval_in_time,"Subtract");
                    if(strtotime($new_sign_in_time) < strtotime($dayLimits['start_of_day'])){
                        $new_sign_in_time =  $dayLimits['start_of_day'];   
                    }
                    // check the new sign in time status
                    SignInOut::overRideSignInOut($sign_in_out->id,$new_sign_in_time,$sign_in_out->id);


                }
            }
        }*/
        
    }

    public function reject($id) {
        $req = Req::find($id);
        $req->status = 0;
        $req->save();
        Helpers::sendReqEmail($req);
    }

    public function cancel($id) {
        $req = Req::find($id);
        $dayLimits = Helpers::getDayLimits(date('Y-m-d H:i:s'),$req->emp_id);
        $today = $dayLimits['start_of_day'];
        if($req->type == "Permissions" || $req->type == "Half Day" || $req->type == "Home"){
            $req->delete();
        }
        if($req->type == "Emergengy Leaves" || $req->type == "Normal Leaves"){
            // if the user did not use any day from the leave 
            if(strtotime($req->time_from) > strtotime($today)){
                $req->delete();
            // user has already  taken part of the leave
            }else{

                $req->time_to = $today;
                $req->actual_time_to = $dayLimits['end_of_day'];
                $req->save();
            }
        }

    }
}
