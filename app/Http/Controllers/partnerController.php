<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\partner;
use App\User;
use App\Company;
use Hash;

class partnerController extends Controller
{
    public function index() {
    	$partners = User::where('admin_show',1)->where('role_id',1)->get();
    	
        $data = [];
        $data['partialView'] = 'partners.list';
        $data['partners'] = $partners;
        
        return view('partners.base', $data);
    }

    

   

    public function init(){
    	$partner = new User();
    	$partner->save();
        return redirect('/admin/partners/'.$partner->id.'/edit');
    	/*$companies = Company::where('admin_show',1)->orderBy('name')->get();
        $data = [];
       
        $data['companies'] = $companies;
        $data['partner'] = $partner;
        $data['partialView'] = 'partners.form';
        return view('partners.base', $data);*/
    }

    public function edit($id){
    	$partner =  User::find($id);
    	$partner->save();
    	$companies = Company::where('admin_show',1)->orderBy('name')->get();
        $data = [];
       
        $data['companies'] = $companies;
        $data['partner'] = $partner;
        $data['partialView'] = 'partners.form';
        return view('partners.base', $data);
    }
    

    public function save(Request $request){
        $data = $request->input();
        //Validations
        $check_email = User::where('email',$data['email'])->where('id','!=',$data['id'])->count();
        if($check_email !=0){
            $data = [];
            $data['status'] = 'error';
            
            $data['msg'] = "Email already exists";
            return response()->json(
                        $data
            );
        }
        $partner = User::find($data['id']);
        $partner->company_id = $data['company_id'];
        $partner->name = $data['name'];
        $partner->email = $data['email'];
        $partner->phone = $data['phone'];
        $partner->user_type = "Owner";
        $partner->admin_show = 1;
        $partner->role_id = 1;
        $partner->password = Hash::make($data['password']);
        $partner->dec_password = $data['password'];
        if($partner->save()){
            $data = [];
            $data['status'] = 'success';
            $data['page'] = '/admin/partners';
            $data['msg'] = "Saved Successfully";
            return response()->json(
                        $data
            );  
        }else{
        	$data = [];
            $data['status'] = 'error';
            $data['page'] = 'none';
            $data['msg'] = "There was an error";
            return response()->json(
                        $data
            );  

        }
        
        

        
        
    }
    
    

    public function delete($id){
       $delete = User::find($id)->delete();
    }
}
