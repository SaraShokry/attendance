<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Holiday_profile extends Model
{
    public function getHolidays(){
    	return $this->hasMany('App\Holidays_profile','holiday_profile_id','id');
    }
}
