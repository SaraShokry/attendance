<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Holiday extends Model
{
    public function getOffice(){
        
        return $this->belongsTo('App\Office', 'office_id', 'id');

    }
}
