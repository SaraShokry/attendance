<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Balance extends Model
{
    protected $table = "balance";

    public function getEmployee(){
    	return $this->hasOne('App\User','id','emp_id');
    }
}
