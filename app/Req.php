<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Req extends Model
{
    protected $table = "requests";

    public function getEmployee(){
    	return $this->hasOne('App\User','id','emp_id');
    }
}
