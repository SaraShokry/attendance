<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Att_profile extends Model
{
	protected $guarded = ['id'];
	
    public function getOffice(){
        return $this->belongsTo('App\Office', 'office_id', 'id');
    }

    public function getHolidayProfile(){
        return $this->belongsTo('App\Holiday_profile', 'holiday_profile_id', 'id');
    }

    public function getWorkOnHolidayProfile(){
        return $this->belongsTo('App\Att_profile', 'work_on_holiday_profile_id', 'id');
    }

    public function getFirstHalfProfile(){
        return $this->belongsTo('App\Att_profile', 'first_half_day_profile_id', 'id');
    }
    public function getSecondHalfProfile(){
        return $this->belongsTo('App\Att_profile', 'second_half_day_profile_id', 'id');
    }
}
