<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Holidays_profile extends Model
{
    public function getHoliday(){
    	return $this->hasOne('App\Holiday','id','holiday_id');
    }
}
