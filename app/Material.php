<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    private $materialFile = null;


    public static function boot() {
        parent::boot();
        static::deleting(function ($course) {
            
            $course->deleteMaterialFile();
            
            
            
            
        });
    }
    public function getCourse(){
        return $this->hasOne('App\Course','id','course_id');
    }
    public function materialFile() {
        if (is_null($this->content)) {
            return null;
        }
        if (is_null($this->materialFile)) {
            $this->materialFile = File::find($this->content);
            return $this->materialFile;
        }
        return $this->materialFile;
    }

    public function deleteMaterialFile() {
        if ($this->materialFile()) {
            $this->materialFile()->delete();
            $this->content = null;
            $this->save();
        }
    }
}
