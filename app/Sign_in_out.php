<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sign_in_out extends Model
{
    protected $table ="sign_in";

    public function getEmployee(){
    	return $this->belongsTo('App\User','emp_id','id');
    }
}
