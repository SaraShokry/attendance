<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penalty extends Model
{
    public function getEmployee(){
    	return $this->hasOne('App\User','id','emp_id');
    }
}
