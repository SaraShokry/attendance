<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    public function getOffice(){
        
        return $this->belongsTo('App\Office', 'office_id', 'id');

    }
}
