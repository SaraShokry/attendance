<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Office extends Model
{
    public function getCompany(){
        
        return $this->belongsTo('App\Company', 'company_id', 'id');

    }
}
